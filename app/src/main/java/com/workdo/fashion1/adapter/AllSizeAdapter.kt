package com.workdo.fashion1.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.workdo.fashion1.R
import com.workdo.fashion1.model.ColorModel

class AllSizeAdapter(
    var context: Activity,
    private val mList: List<ColorModel>,
    private val onFilterClick: (String, String) -> Unit
) : RecyclerView.Adapter<AllSizeAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.cell_size, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val categoriesModel = mList[position]
        if (categoriesModel.id == 0) {
            holder.card.background =
                ResourcesCompat.getDrawable(context.resources, R.drawable.bg_lightpink, null)
            holder.textsize.setTextColor(
                AppCompatResources.getColorStateList(
                    context, R.color.white
                )
            )
        } else {
            holder.card.background =
                ResourcesCompat.getDrawable(context.resources, R.drawable.bg_white_return, null)
            holder.textsize.setTextColor(
                AppCompatResources.getColorStateList(
                    context, R.color.appcolor
                )
            )
        }
        holder.textsize.text = categoriesModel.cateName
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val textsize: TextView = itemView.findViewById(R.id.tvsize)
        val card: ConstraintLayout = itemView.findViewById(R.id.card)

    }

    private fun onFilterClick(id: String, name: String) {
        onFilterClick.invoke(id, name)
    }
}