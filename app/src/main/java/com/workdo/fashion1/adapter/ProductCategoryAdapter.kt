package com.workdo.fashion1.adapter

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.fashion1.R
import com.workdo.fashion1.api.ApiClient
import com.workdo.fashion1.databinding.CellFeaturedProductsBinding
import com.workdo.fashion1.databinding.CellProductsBinding
import com.workdo.fashion1.model.FeaturedProductsSub
import com.workdo.fashion1.utils.Constants
import com.workdo.fashion1.utils.ExtensionFunctions.hide
import com.workdo.fashion1.utils.ExtensionFunctions.show
import com.workdo.fashion1.utils.SharePreference
import com.workdo.fashion1.utils.Utils

class ProductCategoryAdapter(
    private val context: Activity,
    private val providerList: ArrayList<FeaturedProductsSub>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<ProductCategoryAdapter.FeaturedViewHolder>() {


    inner class FeaturedViewHolder(private val binding: CellProductsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: FeaturedProductsSub,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {
            val currency = SharePreference.getStringPref(context, SharePreference.currency_name).toString()

            Log.e("Cuurent", currency)
            Glide.with(context).load(ApiClient.ImageURL.BASE_URL.plus(data.coverImagePath)).into(binding.ivfeaturedProduct)
            binding.tvFeaturedProductName.text = data.name.toString()
            binding.tvFeaturedProductPrice.text = Utils.getPrice(data.finalPrice.toString())
            binding.tvcurrenytype.text = currency

            binding.tvTag.text = data.tagApi
            if (data.inWhishlist == true) {
                binding.ivWishList.background = ResourcesCompat.getDrawable(context.resources, R.drawable.ic_wishlist, null)
            } else {
                binding.ivWishList.background = ResourcesCompat.getDrawable(context.resources, R.drawable.ic_diswishlist, null)
            }

            if(Utils.isLogin(context))
            {
                binding.ivWishList.show()
            } else
            {
                binding.ivWishList.hide()

            }

            binding.ivAddtocard.setOnClickListener {
                itemClick(position, Constants.CartClick)
            }
            binding.ivWishList.setOnClickListener {
                itemClick(position, Constants.FavClick)
            }
            itemView.setOnClickListener {
                itemClick(position, Constants.ItemClick)
            }


        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeaturedViewHolder {
        val view = CellProductsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return FeaturedViewHolder(view)
    }

    override fun onBindViewHolder(holder: FeaturedViewHolder, position: Int) {
        holder.bind(providerList[position], context, position, itemClick)
    }

    override fun getItemCount(): Int {

        return providerList.size
    }
}