package com.workdo.fashion1.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.fashion1.R
import com.workdo.fashion1.model.CategoriesModel
import com.workdo.fashion1.ui.activity.ActProductDetails
import com.workdo.fashion1.utils.Constants

class ProductAdapter(
    var context: Activity,
    private val mList: List<CategoriesModel>,
    private val onFilterClick: (String, String) -> Unit
) : RecyclerView.Adapter<ProductAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.cell_products, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val categoriesModel = mList[position]
        if (categoriesModel.image==0){}else{
        Glide.with(context)
            .load(categoriesModel.image).into(holder.image)}
        holder.itemView.setOnClickListener {
            val intent = Intent(context, ActProductDetails::class.java)
            intent.putExtra(Constants.ProductId,mList[position].id.toString())
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val image: ImageView = itemView.findViewById(R.id.image)
    }

    private fun onFilterClick(id: String, name: String) {
        onFilterClick.invoke(id, name)
    }
}