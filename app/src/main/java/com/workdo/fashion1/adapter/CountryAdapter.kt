package com.workdo.fashion1.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.workdo.fashion1.databinding.RowDlgItemListBinding
import com.workdo.fashion1.model.CountryDataItem
import com.workdo.fashion1.model.StateListData

class CountryAdapter(var countryList:ArrayList<CountryDataItem>,var itemClick:(CountryDataItem) -> Unit):RecyclerView.Adapter<CountryAdapter.CountryViewHolder>() {


    inner class CountryViewHolder (var itemBinding:RowDlgItemListBinding):RecyclerView.ViewHolder(itemBinding.root)
    {
        fun bindItems(data:CountryDataItem,itemClick:(CountryDataItem) -> Unit)= with(itemBinding)
        {
            itemBinding.tvItemName.text=data.name.toString()
            itemView.setOnClickListener {
                itemClick(data)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountryViewHolder {
        val view=RowDlgItemListBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return CountryViewHolder(view)
    }

    override fun onBindViewHolder(holder: CountryViewHolder, position: Int) {

        holder.bindItems(countryList[position],itemClick)

    }

    override fun getItemCount(): Int {
        return countryList.size
    }
}