package com.workdo.fashion1.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.workdo.fashion1.R
import com.workdo.fashion1.model.CountryDataItem

class countryad(context: Context, zonas: List<CountryDataItem>) :
    ArrayAdapter<CountryDataItem>(context, 0, zonas) {


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        //return super.getView(position, convertView, parent)
        val vista = convertView ?: LayoutInflater.from(context)
            .inflate(R.layout.esqueleto_personas, parent, false)
        getItem(position)?.let { persona ->
            vista.findViewById<TextView>(R.id.textviewEdad).apply {

                text = persona.name

            }
        }

        return vista
    }
}