package com.workdo.fashion1.utils;

import com.workdo.fashion1.model.CityListData;

public interface OnItemClickListenerGuestCity {
    void onItemClickCityGuest(CityListData item);
}
