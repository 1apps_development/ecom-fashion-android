package com.workdo.fashion1.model

data class CartModel(
    var product_id:String,
    var image:String,
    var name:String,
    var orignal_price:String,
    var discount_price:String,
    var final_price:String,
    var qty:String,
    var variant_id:String,
    var variant_name:String)
