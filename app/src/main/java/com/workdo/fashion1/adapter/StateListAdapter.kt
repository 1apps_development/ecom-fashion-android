package com.workdo.fashion1.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.workdo.fashion1.databinding.RowDlgItemListBinding
import com.workdo.fashion1.model.StateListData

class StateListAdapter(var stateList:ArrayList<StateListData>,var itemClick:(StateListData) -> Unit):RecyclerView.Adapter<StateListAdapter.StateListViewHolder>() {

    inner class StateListViewHolder(val binding:RowDlgItemListBinding):RecyclerView.ViewHolder(binding.root)
    {
        fun bindItems(data:StateListData,itemClick:(StateListData) -> Unit)= with(binding)
        {
            binding.tvItemName.text=data.name.toString()
            itemView.setOnClickListener {
                itemClick(data)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StateListViewHolder {
        val view=RowDlgItemListBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return StateListViewHolder(view)
    }

    override fun onBindViewHolder(holder: StateListViewHolder, position: Int) {
        holder.bindItems(stateList[position], itemClick =itemClick)
    }

    override fun getItemCount(): Int {
        return stateList.size
    }
}