package com.workdo.fashion1.ui.activity

import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.workdo.fashion1.R
import com.workdo.fashion1.adapter.PaymentAdapter
import com.workdo.fashion1.api.ApiClient
import com.workdo.fashion1.base.BaseActivity
import com.workdo.fashion1.databinding.ActPaymentBinding
import com.workdo.fashion1.model.PaymentData
import com.workdo.fashion1.remote.NetworkResponse
import com.workdo.fashion1.ui.authentication.ActWelCome
import com.workdo.fashion1.ui.option.ActCart
import com.workdo.fashion1.utils.ExtensionFunctions.hide
import com.workdo.fashion1.utils.ExtensionFunctions.show
import com.workdo.fashion1.utils.SharePreference
import com.workdo.fashion1.utils.Utils
import kotlinx.coroutines.launch

open class ActPayment : BaseActivity() {
    private lateinit var _binding: ActPaymentBinding
    private var paymentList = ArrayList<PaymentData>()
    private lateinit var paymentAdapter: PaymentAdapter
    private var manager: GridLayoutManager? = null
    var comment = ""
    var paymentName = ""
    var stripeKey = ""
    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActPaymentBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        //callPaymentList()
        Log.e("stripeKey", stripeKey)


        _binding.ivBack.setOnClickListener { finish() }
        _binding.chbTermsCondition.isChecked = false
        _binding.chbTermsCondition.setOnClickListener {
            _binding.btnContinue.isEnabled = _binding.chbTermsCondition.isChecked
        }

        _binding.btnContinue.setOnClickListener {
            if (_binding.chbTermsCondition.isChecked) {
                    comment = _binding.edNote.text.toString()
                    SharePreference.setStringPref(this@ActPayment, SharePreference.Payment_Comment, comment)
                    openActivity(ActConfirm::class.java)
            }
        }
        _binding.clcart.setOnClickListener { openActivity(ActCart::class.java) }
        manager = GridLayoutManager(this@ActPayment, 1, GridLayoutManager.VERTICAL, false)
        _binding.tvTerms.setOnClickListener {
            val terms = SharePreference.getStringPref(this@ActPayment, SharePreference.Terms).toString()
            val uri: Uri = Uri.parse(terms)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            intent.addCategory(Intent.CATEGORY_BROWSABLE)
            startActivity(intent)
        }
//        paymentSheet = PaymentSheet(this@ActPayment, ::onPaymentSheetResult)


    }

    /*private fun stripe() {
        PaymentConfiguration.init(
            this@ActPayment,
            stripeKey
        )


        createCustomers()
    }*/


    private fun callPaymentList() {
        Utils.showLoadingProgress(this@ActPayment)
        lifecycleScope.launch {
            val request=HashMap<String,String>()
            request["theme_id"]=resources.getString(R.string.theme_id)
            when (val response = ApiClient.getClient(this@ActPayment)
                .paymentList(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val paymentListResponse = response.body
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.size ?: 0) > 0) {
                                _binding.rvPayment.show()
                                _binding.vieww.hide()

                                runOnUiThread {
                                    paymentListResponse.data?.let {
                                        paymentList.addAll(it)
                                    }
                                }
                                paymentList.removeAll {
                                    it.status == "off"
                                }
                                paymentListAdapter(paymentList)

                               /* for (element in paymentList) {
                                    if (element.nameString == "Stripe") {
                                        Log.e("stripeKeyy", element.stripePublishableKey.toString())

                                        stripeKey = element.stripePublishableKey.toString()
                                        SharePreference.setStringPref(
                                            this@ActPayment,
                                            SharePreference.stripeSecretKey,
                                            element.stripeSecretKey.toString()
                                        )

                                    }
                                }*/

                               // stripe()
                            } else {
                                _binding.rvPayment.hide()
                                _binding.vieww.show()
                            }
                            paymentAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActPayment,
                                paymentListResponse.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActPayment,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActPayment)
                    } else {
                        Utils.errorAlert(
                            this@ActPayment,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActPayment, resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActPayment, "Something went wrong")
                }
            }
        }
    }

    private fun paymentListAdapter(paymentList: ArrayList<PaymentData>) {
        _binding.rvPayment.layoutManager = manager
        paymentAdapter = PaymentAdapter(this@ActPayment, paymentList) { i: Int, s: String ->
                if (paymentList[i].isSelect == true) {
                    paymentName = paymentList[i].nameString.toString()
                    SharePreference.setStringPref(this@ActPayment, SharePreference.Payment_Type, paymentName)
                    SharePreference.setStringPref(this@ActPayment, SharePreference.PaymentImage, paymentList[i].image.toString())
                } else {

                }
            }
        _binding.rvPayment.adapter = paymentAdapter
    }

    override fun onResume() {
        super.onResume()
        paymentList.clear()
        callPaymentList()
    }
}