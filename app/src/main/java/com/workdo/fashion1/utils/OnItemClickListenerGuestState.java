package com.workdo.fashion1.utils;

import com.workdo.fashion1.model.StateListData;

public interface OnItemClickListenerGuestState {
    void onItemClickStateGuest(StateListData item);
}
