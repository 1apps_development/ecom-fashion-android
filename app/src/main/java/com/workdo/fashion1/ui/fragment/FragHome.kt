package com.workdo.fashion1.ui.fragment

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.util.Log
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.workdo.fashion1.R
import com.workdo.fashion1.adapter.*
import com.workdo.fashion1.api.ApiClient
import com.workdo.fashion1.base.BaseFragment
import com.workdo.fashion1.databinding.DlgConfirmBinding
import com.workdo.fashion1.databinding.FragHomeBinding
import com.workdo.fashion1.model.*
import com.workdo.fashion1.remote.NetworkResponse
import com.workdo.fashion1.ui.activity.ActCategoryProduct
import com.workdo.fashion1.ui.activity.ActLoyality
import com.workdo.fashion1.ui.activity.ActProductDetails
import com.workdo.fashion1.ui.activity.ActShoppingCart
import com.workdo.fashion1.ui.authentication.ActWelCome
import com.workdo.fashion1.ui.option.ActMenu
import com.workdo.fashion1.ui.option.ActSearch
import com.workdo.fashion1.utils.Constants
import com.workdo.fashion1.utils.ExtensionFunctions.hide
import com.workdo.fashion1.utils.ExtensionFunctions.show
import com.workdo.fashion1.utils.PaginationScrollListener
import com.workdo.fashion1.utils.SharePreference
import com.workdo.fashion1.utils.Utils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.reflect.Type


class FragHome : BaseFragment<FragHomeBinding>() {

    private lateinit var _binding: FragHomeBinding

    internal var isLoading = false
    internal var isLastPage = false
    private var currentPage = 1
    private var total_pages: Int = 0

    internal var isLoadingCategorirs = false
    internal var isLastPageCategorirs = false
    private var currentPageCategorirs = 1
    private var total_pagesCategorirs: Int = 0

    internal var isLoadingBestSeller = false
    internal var isLastPageBestSeller = false
    private var currentPageBestSeller = 1
    private var total_pagesBestSeller: Int = 0


    var count = 1
    var qtyoffline = "1"


    internal var isLoadingTrending = false
    internal var isLastPageTrending = false
    private var currentPageTrending = 1
    private var total_pagesTrending: Int = 0


    private var featuredProductsList = ArrayList<FeaturedProducts>()
    private lateinit var categoriesAdapter: CategoriesAdapter
    private var featuredProductsSubList = ArrayList<FeaturedProductsSub>()
    private lateinit var categoriesProductAdapter: FeaturedAdapter
    private var homeCategoriesList = ArrayList<HomeCategoriesItem>()
    private lateinit var allCategoriesAdapter: AllCateAdapter
    private var manager: GridLayoutManager? = null
    private var managerAllCategories: GridLayoutManager? = null
    private var managerBestsellers: GridLayoutManager? = null
    private var bestsellersList = ArrayList<FeaturedProductsSub>()
    private lateinit var bestSellersAdapter: BestsellerAdapter

    private lateinit var trendingcategoriesAdapter: TrendingCategoriesAdapter
    private var trendingcategoriesList = ArrayList<TrendingCategoriData>()
    private var trendingProductsSubList = ArrayList<FeaturedProductsSub>()
    private lateinit var trendingProductAdapter: TrendingProductAdapter
    private var managerTrending: GridLayoutManager? = null

    var subcategory_id: String = ""
    var maincategory_id: String = ""
    var trendingcategory_id: String = ""
    var searched = ""
    var currency = ""
    var currency_name = ""

    var searchHint = ""
    override fun initView(view: View) {
        _binding = FragHomeBinding.bind(_binding.root)
        init()
    }

    override fun getBinding(): FragHomeBinding {
        _binding = FragHomeBinding.inflate(layoutInflater)
        return _binding
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun init() {

        manager = GridLayoutManager(requireActivity(), 1, GridLayoutManager.HORIZONTAL, false)
        managerBestsellers =
            GridLayoutManager(requireActivity(), 2, GridLayoutManager.VERTICAL, false)
        managerAllCategories =
            GridLayoutManager(requireActivity(), 2, GridLayoutManager.VERTICAL, false)
        managerTrending =
            GridLayoutManager(requireActivity(), 1, GridLayoutManager.HORIZONTAL, false)

        featuredProductsAdapter()
        trendingProductCateAdapter()
        pagination()
        paginationTrending()
        _binding.btnShowMoreCategories.setOnClickListener {
            isLoadingCategorirs = true
            currentPageCategorirs++
            callCategories()
            //  paginationCategories()
        }
        _binding.btnShowMoreBestSellers.setOnClickListener {
            Log.e("ClickBestseller", "bestsellerList")
            isLoadingBestSeller = true
            currentPageBestSeller++
            callBestseller()
            // paginationBestSeller()
        }


        _binding.tvshowmore.setOnClickListener {
            if (SharePreference.getBooleanPref(
                    requireActivity(),
                    SharePreference.isLogin
                )
            ) {

                openActivity(ActLoyality::class.java)

            } else {
                Utils.setInvalidToekn(requireActivity())
            }
        }

        if (SharePreference.getStringPref(requireActivity(), SharePreference.cartCount)
                .isNullOrEmpty()
        ) {
            SharePreference.setStringPref(requireActivity(), SharePreference.cartCount, "0")
        }



        _binding.ivMenu.setOnClickListener { openActivity(ActMenu::class.java) }
        /* _binding.btnShowMoreCategories.setOnClickListener {
             val intent = Intent(requireActivity(), MainActivity::class.java)
             intent.putExtra("pos", "3")
             startActivity(intent)
         }*/
        /* _binding.btnShowMoreBestSellers.setOnClickListener {
             openActivity(ActBestsellers::class.java)
         }*/
        _binding.clSearch.setOnClickListener {
            startActivity(
                Intent(requireActivity(), ActSearch::class.java).putExtra(
                    "Searched",
                    searched
                )
            )
        }
        _binding.edSearch.setOnClickListener {
            startActivity(
                Intent(requireActivity(), ActSearch::class.java).putExtra(
                    "Searched",
                    searched
                )
            )
        }
        _binding.clcart.setOnClickListener {
            openActivity(ActShoppingCart::class.java)
        }
    }

    private fun paginationTrending() {
        val paginationListener = object : PaginationScrollListener(managerTrending) {
            override fun isLastPage(): Boolean {
                return isLastPageTrending
            }

            override fun isLoading(): Boolean {
                return isLoadingTrending
            }

            override fun loadMoreItems() {
                isLoadingTrending = true
                currentPageTrending++
                callTrendingCategorysProduct()
            }
        }
        _binding.rvTrandingCategoryProduct.addOnScrollListener(paginationListener)
    }

    //Featured Product Api calling
    private fun callTrendingProduct() {
        Utils.showLoadingProgress(requireActivity())
        val request = HashMap<String, String>()
        request["theme_id"] = resources.getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setTrandingCategory(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val trendingcategorieResponse = response.body.data
                    when (response.body.status) {
                        1 -> {

                            response.body.data?.let { trendingcategoriesList.addAll(it) }

                            if (trendingcategoriesList.size == 0) {
                                _binding.rvTrandingCategory.hide()

                            } else {
                                _binding.rvTrandingCategory.show()

                            }
                            trendingcategoriesAdapter.notifyDataSetChanged()

                            trendingcategory_id = trendingcategoriesList[0].id.toString()
                            currentPageTrending = 1
                            isLoadingTrending = false
                            isLastPageTrending = false
                            callTrendingCategorysProduct()

                            Utils.dismissLoadingProgress()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                trendingcategorieResponse?.get(0)?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                trendingcategorieResponse?.get(0)?.message.toString()
                            )
                            startActivity(Intent(requireActivity(), ActWelCome::class.java))
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
                else -> {}
            }
        }
    }


    private fun trendingProductCateAdapter() {
        _binding.rvTrandingCategory.layoutManager =
            GridLayoutManager(requireActivity(), 2, GridLayoutManager.HORIZONTAL, false)
        trendingcategoriesAdapter =
            TrendingCategoriesAdapter(
                requireActivity(),
                trendingcategoriesList
            ) { id: String, name: String ->
                Log.e("CategoriesName", name)
                Log.e("CategoriesName", id.toString())
                trendingProductsSubList.clear()
                trendingcategory_id = id.toString()
                currentPageTrending = 1
                total_pagesTrending = 0
                isLastPageTrending = false
                isLoadingTrending = false
                callTrendingCategorysProduct()
            }
        _binding.rvTrandingCategory.adapter = trendingcategoriesAdapter
    }

    //Categories Product Api calling
    private fun callTrendingCategorysProduct() {
        Utils.showLoadingProgress(requireActivity())
        val trendingProduct = HashMap<String, String>()
        trendingProduct["main_category_id"] = trendingcategory_id
        trendingProduct["theme_id"] = resources.getString(R.string.theme_id)

        lifecycleScope.launch {
            val response =
                if (SharePreference.getStringPref(requireActivity(), SharePreference.token)
                        ?.isEmpty() == true
                ) {
                    ApiClient.getClient(requireActivity())
                        .setTrendingProductGuest(currentPageTrending.toString(), trendingProduct)
                } else {
                    ApiClient.getClient(requireActivity())
                        .setTrendingProduct(currentPageTrending.toString(), trendingProduct)
                }


            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val trendingProductResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                _binding.rvTrandingCategoryProduct.show()
                                _binding.view.hide()
                                currentPageTrending =
                                    trendingProductResponse?.currentPage!!.toInt()
                               total_pagesTrending =
                                    trendingProductResponse.lastPage!!.toInt()
                                trendingProductResponse.data?.let {
                                    trendingProductsSubList.addAll(it)
                                }
                                if(trendingcategoriesList.size>0)
                                {
                                    trendingcategoriesList[0].isSelect = true

                                }

                                if (currentPageTrending >= total_pagesTrending) {
                                    isLastPageTrending = true
                                }
                                isLoadingTrending = false
                            } else {
                                _binding.rvTrandingCategoryProduct.hide()
                                _binding.view.show()
                            }
                            trendingProductAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                trendingProductResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                trendingProductResponse?.message.toString()
                            )
                            startActivity(Intent(requireActivity(), ActWelCome::class.java))
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
                else -> {}
            }
        }
    }

    private fun trendingProductAdapter(trendingProductsSubLists: ArrayList<FeaturedProductsSub>) {
        _binding.rvTrandingCategoryProduct.layoutManager = managerTrending
        trendingProductAdapter =
            TrendingProductAdapter(
                requireActivity(),
                trendingProductsSubLists
            ) { i: Int, s: String ->
                if (s == Constants.FavClick) {
                    if (SharePreference.getBooleanPref(
                            requireActivity(),
                            SharePreference.isLogin
                        )
                    ) {
                        Log.e("FavClick", "FavClick")
                        if (trendingProductsSubLists[i].inWhishlist == false) {
                            callWishlist(
                                "add", trendingProductsSubLists[i].id, "TrendingList", i
                            )
                        } else {
                            callWishlist(
                                "remove",
                                trendingProductsSubLists[i].id,
                                "TrendingList",
                                i
                            )
                        }
                    } else {
                        openActivity(ActWelCome::class.java)
                        activity?.finish()
                    }
                } else if (s == Constants.CartClick) {
                    val data = trendingProductsSubList[i]
                    if (!Utils.isLogin(requireActivity())) {
                        guestUserAddToCart(data, trendingProductsSubLists[i].id)
                    } else {
                        val addtocart = HashMap<String, String>()
                        addtocart["user_id"] =
                            SharePreference.getStringPref(requireActivity(), SharePreference.userId)
                                .toString()
                        addtocart["product_id"] = trendingProductsSubLists[i].id.toString()
                        addtocart["variant_id"] =
                            trendingProductsSubLists[i].defaultVariantId.toString()
                        addtocart["qty"] = "1"
                        addtocart["theme_id"] = resources.getString(R.string.theme_id)

                        addtocartApi(
                            addtocart,
                            trendingProductsSubLists[i].id,
                            trendingProductsSubLists[i].defaultVariantId, i
                        )
                    }
                } else if (s == Constants.ItemClick) {
                    val intent = Intent(requireActivity(), ActProductDetails::class.java)
                    intent.putExtra(Constants.ProductId, trendingProductsSubLists[i].id.toString())
                    startActivity(intent)
                }
            }
        _binding.rvTrandingCategoryProduct.adapter = trendingProductAdapter
    }


    //TODO Currency Api calling
    private fun callCurrencyApi() {
        Utils.showLoadingProgress(requireActivity())
        val request = HashMap<String, String>()
        request["theme_id"] = resources.getString(R.string.theme_id)

        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setcurrency(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val currencyResponse = response.body.data
                    when (response.body.status) {
                        1 -> {

                            Log.e("Currency", currencyResponse?.currency.toString())
                            Log.e("Currency", currencyResponse?.currency_name.toString())
                            SharePreference.setStringPref(
                                requireActivity(),
                                SharePreference.currency,
                                currencyResponse?.currency.toString()
                            )
                            SharePreference.setStringPref(
                                requireActivity(),
                                SharePreference.currency_name,
                                currencyResponse?.currency_name.toString()
                            )
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                currencyResponse?.message.toString()
                            )
                        }
                        9 -> {
                            startActivity(Intent(requireActivity(), ActWelCome::class.java))
                            // openActivity(ActWelCome::class.java)
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
                else -> {}
            }
        }
    }

    //TODO Categories Product pagination
    private fun pagination() {
        val paginationListener = object : PaginationScrollListener(manager) {
            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                isLoading = true
                currentPage++
                callCategorysProduct()
            }
        }
        _binding.rvFeaturedProduct.addOnScrollListener(paginationListener)
    }

    //Adapter set Categories Product
    private fun categoriesProductsAdapter(featuredProductsSubList: ArrayList<FeaturedProductsSub>) {
        _binding.rvFeaturedProduct.layoutManager = manager
        categoriesProductAdapter =
            FeaturedAdapter(requireActivity(), featuredProductsSubList) { i: Int, s: String ->
                if (s == Constants.FavClick) {
                    if (SharePreference.getBooleanPref(
                            requireActivity(),
                            SharePreference.isLogin
                        )
                    ) {
                        Log.e("FavClick", "FavClick")
                        if (featuredProductsSubList[i].inWhishlist == false) {
                            callWishlist(
                                "add",
                                featuredProductsSubList[i].id,
                                "CategoriesProduct",
                                i
                            )
                        } else {
                            callWishlist(
                                "remove",
                                featuredProductsSubList[i].id,
                                "CategoriesProduct", i
                            )
                        }
                    } else {
                        openActivity(ActWelCome::class.java)
                        activity?.finish()
                    }
                } else if (s == Constants.CartClick) {
                    val data = featuredProductsSubList[i]
                    if (!Utils.isLogin(requireActivity())) {
                        guestUserAddToCart(data, featuredProductsSubList[i].id)
                    } else {
                        val addtocart = HashMap<String, String>()
                        addtocart["user_id"] =
                            SharePreference.getStringPref(requireActivity(), SharePreference.userId)
                                .toString()
                        addtocart["product_id"] = featuredProductsSubList[i].id.toString()
                        addtocart["variant_id"] =
                            featuredProductsSubList[i].defaultVariantId.toString()
                        addtocart["qty"] = "1"
                        addtocart["theme_id"] = resources.getString(R.string.theme_id)

                        addtocartApi(
                            addtocart,
                            featuredProductsSubList[i].id,
                            featuredProductsSubList[i].defaultVariantId, i
                        )
                    }
                } else if (s == Constants.ItemClick) {
                    val intent = Intent(requireActivity(), ActProductDetails::class.java)
                    intent.putExtra(Constants.ProductId, featuredProductsSubList[i].id.toString())
                    startActivity(intent)
                }
            }
        _binding.rvFeaturedProduct.adapter = categoriesProductAdapter
    }

    private fun guestUserAddToCart(data: FeaturedProductsSub, id: Int?) {
        Log.e("Position", id.toString())
        val cartData = cartData(data)
        Log.e("cartData2", Gson().toJson(cartData))

        val cartListData = SharePreference.getStringPref(
            requireActivity(),
            SharePreference.GuestCartList
        )
        val type: Type = object : TypeToken<ArrayList<ProductListItem>>() {}.type

        var cartList =
            Gson().fromJson<ArrayList<ProductListItem>>(cartListData, type)
        Log.e("cartListBefore2", Gson().toJson(cartList))

        if (cartList == null) {
            //  dlgConfirm(data.name+" "+R.string.addsuccess)
            val cartDataList = ArrayList<ProductListItem>()
            cartDataList.add(cartData)

            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.GuestCartList,
                Gson().toJson(cartDataList)
            )
            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.cartCount,
                cartDataList.size.toString()
            )
            _binding.tvCount.text = cartDataList.size.toString()
        } else if (isProductAlreadyAdded(cartList, data.id.toString())) {
            // Utils.errorAlert(requireActivity(), "item already added into cart")
            for (i in 0 until cartList.size) {
                if (cartList[i].productId?.toInt() == id) {
                    val position = cartList[i]
                    Log.e("SelectId", position.toString())
                    dlgAlreadyCart(data.message, data.id, data.defaultVariantId, i)
                }
            }
        } else {
            dlgConfirm(data.name.plus(" ").plus("add successfully."))
            cartList.add(cartData)
            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.GuestCartList,
                Gson().toJson(cartList)
            )
            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.cartCount,
                cartList.size.toString()
            )
            _binding.tvCount.text = cartList.size.toString()
        }
    }

    private fun addtocartApi(
        addtocart: HashMap<String, String>,
        id: Int?,
        defaultVariantId: Int?,
        i: Int?
    ) {
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {


            when (val response = ApiClient.getClient(requireActivity())
                .addtocart(addtocart)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val addtocart = response.body.data
                    when (response.body.status) {
                        1 -> {
                            dlgConfirm(response.body.data?.message)
                            SharePreference.setStringPref(
                                requireActivity(),
                                SharePreference.cartCount,
                                addtocart?.count.toString()
                            )
                            setCount(addtocart?.count)
                        }
                        0 -> {
                            dlgAlreadyCart(response.body.data?.message, id, defaultVariantId, i)

                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                addtocart?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
                else -> {}
            }
        }
    }

    private fun setCount(count: Int?) {
        _binding.tvCount.text = count.toString()
    }

    private fun callWishlist(type: String, id: Int?, itemType: String, position: Int) {
        Utils.showLoadingProgress(requireActivity())
        val wishlist = HashMap<String, String>()
        wishlist["user_id"] =
            SharePreference.getStringPref(requireActivity(), SharePreference.userId).toString()
        wishlist["product_id"] = id.toString()
        wishlist["wishlist_type"] = type
        wishlist["theme_id"] = resources.getString(R.string.theme_id)

        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setWishlist(wishlist)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val wishlistResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if (type == "add") {
                                if (itemType == "CategoriesProduct") {
                                    featuredProductsSubList[position].inWhishlist = true
                                    categoriesProductAdapter.notifyItemChanged(position)
                                } else if (itemType == "BestsellersList") {
                                    bestsellersList[position].inWhishlist = true
                                    bestSellersAdapter.notifyItemChanged(position)
                                } else if (itemType == "TrendingList") {
                                    trendingProductsSubList[position].inWhishlist = true
                                    trendingProductAdapter.notifyItemChanged(position)
                                }
                            } else {
                                if (itemType == "CategoriesProduct") {
                                    featuredProductsSubList[position].inWhishlist = false
                                    categoriesProductAdapter.notifyItemChanged(position)
                                } else if (itemType == "BestsellersList") {
                                    bestsellersList[position].inWhishlist = false
                                    bestSellersAdapter.notifyItemChanged(position)
                                } else if (itemType == "TrendingList") {
                                    trendingProductsSubList[position].inWhishlist = false
                                    trendingProductAdapter.notifyItemChanged(position)
                                }
                            }
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                wishlistResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                wishlistResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
                else -> {}
            }
        }
    }


    private fun cartData(data: FeaturedProductsSub): ProductListItem {
        return ProductListItem(
            data.coverImagePath.toString(),
            data.defaultVariantId.toString(),
            data.finalPrice.toString(),
            data.discountPrice.toString(),
            data.id.toString(),
            1,
            data.name.toString(),
            "",
            "",
            data.variantName.toString(),
            data.originalPrice.toString()
        )
    }

    private fun isProductAlreadyAdded(cartList: ArrayList<ProductListItem>, id: String): Boolean {
        return cartList.filter { it.productId == id.toString() }.size == 1
    }

    //adapter set Categorirs
    private fun categoriesAdapter(homeCategoriesList: ArrayList<HomeCategoriesItem>) {
        _binding.rvAllCategories.layoutManager = managerAllCategories
        allCategoriesAdapter =
            AllCateAdapter(requireActivity(), homeCategoriesList) { i: Int, s: String ->
                if (s == Constants.ItemClick) {
                    startActivity(
                        Intent(
                            requireActivity(),
                            ActCategoryProduct::class.java
                        ).putExtra("maincategory_id", homeCategoriesList[i].categoryId.toString())
                    )
                }
            }
        _binding.rvAllCategories.adapter = allCategoriesAdapter
    }

    // adapter set Featured Product
    private fun featuredProductsAdapter() {
        _binding.rvCategories.layoutManager =
            GridLayoutManager(requireActivity(), 2, GridLayoutManager.HORIZONTAL, false)
        categoriesAdapter =
            CategoriesAdapter(
                requireActivity(),
                featuredProductsList
            ) { id: String, name: String, mainId: String ->
                Log.e("CategoriesName", name)
                Log.e("CategoriesName", id)
                Log.e("CategoriesName", mainId)
                subcategory_id = id.toString()
                maincategory_id = mainId.toString()
                featuredProductsSubList.clear()
                currentPage = 1
                total_pages = 0
                isLastPage = false
                isLoading = false
                callCategorysProduct()
            }
        _binding.rvCategories.adapter = categoriesAdapter
    }


    //All Categories pagination
    private fun paginationCategories() {
        val paginationListener = object : PaginationScrollListener(managerAllCategories) {
            override fun isLastPage(): Boolean {
                return isLastPageCategorirs
            }

            override fun isLoading(): Boolean {
                return isLoadingCategorirs
            }

            override fun loadMoreItems() {
                isLoadingCategorirs = true
                currentPageCategorirs++
                callCategories()
            }
        }
        _binding.rvAllCategories.addOnScrollListener(paginationListener)
    }


    private fun callBestseller() {
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            val request = HashMap<String, String>()
            request["theme_id"] = resources.getString(R.string.theme_id)
            val response =
                if (SharePreference.getStringPref(requireActivity(), SharePreference.token)
                        ?.isEmpty() == true
                ) {
                    ApiClient.getClient(requireActivity())
                        .setBestSellerGuest(currentPageBestSeller.toString(), request)
                } else {
                    ApiClient.getClient(requireActivity())
                        .setBestSeller(currentPageBestSeller.toString(), request)
                }

            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val bestsellerResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                if (Utils.isLogin(requireActivity())) {
                                    SharePreference.setStringPref(
                                        requireActivity(),
                                        SharePreference.cartCount,
                                        response.body.count.toString()
                                    )
                                    _binding.tvCount.text = response.body.count.toString()
                                }

                                _binding.rvBestsellers.show()
                                if (currentPageBestSeller == response.body.data?.lastPage) {
                                    _binding.btnShowMoreBestSellers.hide()
                                } else {
                                    _binding.btnShowMoreBestSellers.show()
                                }
                                this@FragHome.currentPageBestSeller =
                                    bestsellerResponse?.currentPage!!.toInt()
                                this@FragHome.total_pagesBestSeller =
                                    bestsellerResponse.lastPage!!.toInt()
                                bestsellerResponse.data?.let {
                                    bestsellersList.addAll(it)
                                }


                                if (currentPageBestSeller >= total_pagesBestSeller) {
                                    isLastPageBestSeller = true
                                }
                                isLoadingBestSeller = false
                            } else {
                                _binding.rvBestsellers.hide()
                            }
                            bestSellersAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                bestsellerResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                bestsellerResponse?.data?.get(0)?.message.toString()
                            )
                            Utils.setInvalidToekn(requireActivity())
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        getString(R.string.something_went_wrong)
                    )
                }
                else -> {}
            }
        }
    }

    //Adapter set Best seller
    private fun bestsellerAdapter(bestsellersList: ArrayList<FeaturedProductsSub>) {
        _binding.rvBestsellers.layoutManager = managerBestsellers
        bestSellersAdapter =
            BestsellerAdapter(requireActivity(), bestsellersList) { i: Int, s: String ->
                if (s == Constants.FavClick) {
                    if (SharePreference.getBooleanPref(
                            requireActivity(),
                            SharePreference.isLogin
                        )
                    ) {
                        Log.e("FavClick", "FavClick")
                        if (bestsellersList[i].inWhishlist == false) {
                            callWishlist(
                                "add", bestsellersList[i].id, "BestsellersList", i
                            )
                        } else {
                            callWishlist(
                                "remove",
                                bestsellersList[i].id,
                                "BestsellersList",
                                i
                            )
                        }
                    } else {
                        openActivity(ActWelCome::class.java)
                        activity?.finish()
                    }
                } else if (s == Constants.CartClick) {
                    val data = bestsellersList[i]
                    if (!Utils.isLogin(requireActivity())) {
                        guestUserAddToCart(data, bestsellersList[i].id)
                    } else {
                        val addtocart = HashMap<String, String>()
                        addtocart["user_id"] =
                            SharePreference.getStringPref(requireActivity(), SharePreference.userId)
                                .toString()
                        addtocart["product_id"] = bestsellersList[i].id.toString()
                        addtocart["variant_id"] = bestsellersList[i].defaultVariantId.toString()
                        addtocart["qty"] = "1"
                        addtocart["theme_id"] = resources.getString(R.string.theme_id)

                        addtocartApi(
                            addtocart,
                            bestsellersList[i].id,
                            bestsellersList[i].defaultVariantId, i
                        )
                    }
                } else if (s == Constants.ItemClick) {
                    val intent = Intent(requireActivity(), ActProductDetails::class.java)
                    intent.putExtra(Constants.ProductId, bestsellersList[i].id.toString())
                    startActivity(intent)
                }
            }
        _binding.rvBestsellers.adapter = bestSellersAdapter
    }

    // Categories api callig
    private fun callCategories() {
        Utils.showLoadingProgress(requireActivity())
        val request = HashMap<String, String>()
        request["theme_id"] = resources.getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setHomeCategorys(currentPageCategorirs.toString(), request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val categoriesResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                _binding.rvAllCategories.show()
                                this@FragHome.currentPageCategorirs =
                                    categoriesResponse?.currentPage!!.toInt()
                                this@FragHome.total_pagesCategorirs =
                                    categoriesResponse.lastPage!!.toInt()
                                categoriesResponse.data?.let {
                                    homeCategoriesList.addAll(it)
                                }
                                if (currentPageCategorirs == response.body.data.lastPage) {
                                    _binding.btnShowMoreCategories.hide()
                                } else {
                                    _binding.btnShowMoreCategories.show()
                                }
                                if (currentPageCategorirs >= total_pagesCategorirs) {
                                    isLastPageCategorirs = true
                                }
                                isLoadingCategorirs = false
                            } else {
                                _binding.rvAllCategories.hide()
                            }
                            allCategoriesAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                categoriesResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                categoriesResponse?.message.toString()
                            )
                            startActivity(Intent(requireActivity(), ActWelCome::class.java))
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
                else -> {}
            }
        }
    }

    //Categories Product Api calling
    private fun callCategorysProduct() {
        Utils.showLoadingProgress(requireActivity())
        val categoriesProduct = HashMap<String, String>()
        categoriesProduct["subcategory_id"] = subcategory_id
        categoriesProduct["maincategory_id"] = maincategory_id
        categoriesProduct["theme_id"] = resources.getString(R.string.theme_id)

        lifecycleScope.launch {
            val response =
                if (SharePreference.getStringPref(requireActivity(), SharePreference.token)
                        ?.isEmpty() == true
                ) {
                    ApiClient.getClient(requireActivity())
                        .setCategorysProductGuest(currentPage.toString(), categoriesProduct)
                } else {
                    ApiClient.getClient(requireActivity())
                        .setCategorysProduct(currentPage.toString(), categoriesProduct)
                }

            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val categoriesProductResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                _binding.rvFeaturedProduct.show()
                                this@FragHome.currentPage =
                                    categoriesProductResponse?.currentPage!!.toInt()
                                this@FragHome.total_pages =
                                    categoriesProductResponse.lastPage!!.toInt()
                                categoriesProductResponse.data?.let {
                                    featuredProductsSubList.addAll(it)
                                }
                                if (currentPage >= total_pages) {
                                    isLastPage = true
                                }
                                isLoading = false
                            } else {
                                _binding.rvFeaturedProduct.hide()
                            }
                            categoriesProductAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                categoriesProductResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                categoriesProductResponse?.message.toString()
                            )
                            startActivity(Intent(requireActivity(), ActWelCome::class.java))
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
                else -> {}
            }
        }
    }

    //Featured Product Api calling
    private fun callFeaturedProduct() {
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            val request = HashMap<String, String>()
            request["theme_id"] = resources.getString(R.string.theme_id)

            val response =
                if (SharePreference.getStringPref(requireActivity(), SharePreference.token)
                        ?.isEmpty() == true
                ) {
                    ApiClient.getClient(requireActivity())
                        .setFeaturedProductsGuests(request)
                } else {
                    ApiClient.getClient(requireActivity())
                        .setFeaturedProducts(request)
                }


            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val featuredProductResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            Log.e("featuredProductCall", "Second Api Call")

                            featuredProductsList.add(
                                FeaturedProducts(
                                    true,
                                    "",
                                    "",
                                    "",
                                    "",
                                    "All Products",
                                    "",
                                    "",
                                    "",
                                    0
                                )
                            )
                            response.body.data?.let { featuredProductsList.addAll(it) }
                            if (featuredProductsList.size == 0) {
                                _binding.rvCategories.hide()

                            } else {
                                _binding.rvCategories.show()

                            }
                            categoriesAdapter.notifyDataSetChanged()
                            Utils.dismissLoadingProgress()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                featuredProductResponse?.get(0)?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                featuredProductResponse?.get(0)?.message.toString()
                            )
                            startActivity(Intent(requireActivity(), ActWelCome::class.java))
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
                else -> {}
            }
        }
    }

    //TODO Header content api calling
    private fun callHeaderContentApi() {
        Utils.showLoadingProgress(requireActivity())
        val request = HashMap<String, String>()
        request["theme_id"] = resources.getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setLandingPage(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val headerContenResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            //home-header
                            _binding.tvFeaturedCollection.text =
                                headerContenResponse?.themJson?.homeHeader?.homeHeaderSubText
                            _binding.tvDesc.text =
                                headerContenResponse?.themJson?.homeHeader?.homeHeaderTitleText
                            Glide.with(requireActivity())
                                .load(ApiClient.ImageURL.BASE_URL.plus(headerContenResponse?.themJson?.homeHeader?.homeHeaderBackgroundImage))
                                .into(_binding.ivFirstHome)

                            //home-search
                            _binding.tvSearchProductLabel.text =
                                headerContenResponse?.themJson?.homeSearch?.homeSearchSearchProduct
                            _binding.tvicdesc.text =
                                headerContenResponse?.themJson?.homeSearch?.homeSearchSearchProductIe

                            searched =
                                headerContenResponse?.themJson?.homeSearch?.homeSearchSearchProductIe.toString()

                            //home-featured-product
                            _binding.tvCollection.text =
                                headerContenResponse?.themJson?.homeFeaturedProduct?.homeFeaturedProductSubText
                            _binding.tvFeaturedProduct.text =
                                headerContenResponse?.themJson?.homeFeaturedProduct?.homeFeaturedProductTileText

                            //home-categories-featured-collection
                            _binding.tvFeaturedCollections.text =
                                headerContenResponse?.themJson?.homeCategoriesFeaturedCollection?.homeCategoriesFeaturedCollectionTopText
                            _binding.tvCategories.text =
                                headerContenResponse?.themJson?.homeCategoriesFeaturedCollection?.homeCategoriesFeaturedCollectionTitleText
                            _binding.tvDescc.text =
                                headerContenResponse?.themJson?.homeCategoriesFeaturedCollection?.homeCategoriesFeaturedCollectionDescription
                            _binding.btnShowMoreCategories.text =
                                headerContenResponse?.themJson?.homeCategoriesFeaturedCollection?.homeCategoriesFeaturedCollectionCategoryButtonText

                            //home-bestsellers-featured-collection
                            _binding.tvFeaturedCollectionss.text =
                                headerContenResponse?.themJson?.homeBestsellersFeaturedCollection?.homeBestsellersFeaturedCollectionMainText
                            _binding.tvBestSellers.text =
                                headerContenResponse?.themJson?.homeBestsellersFeaturedCollection?.homeBestsellersFeaturedCollectionTitleText

                            //Trending
                            _binding.tvTrandingCategory.text =
                                headerContenResponse?.themJson?.homeTrendingCollection?.homeTrendingCollectionTitleText
                            _binding.tvTrandingProduct.text =
                                headerContenResponse?.themJson?.homeTrendingCollection?.homeTrendingCollectionMainText

                            /* //home-footer
                             _binding.tvdesccc.text =
                                 headerContenResponse?.themJson?.homeFooter?.homeFooterTitleText
                             _binding.tvmess.text =
                                 headerContenResponse?.themJson?.homeFooter?.homeFooterDescription
                             Glide.with(requireActivity())
                                 .load(ApiClient.ImageURL.BASE_URL.plus(headerContenResponse?.themJson?.homeFooter?.homeFooterBackgroundImage))
                                 .into(_binding.ivHomeFooter)*/


                            if (headerContenResponse?.loyalitySection == "on") {
                                _binding.clLoyality.show()
                                //home-footer
                                _binding.tvdesccc.text =
                                    headerContenResponse?.themJson?.homeFooter?.homeFooterTitleText
                                _binding.tvmess.text =
                                    headerContenResponse?.themJson?.homeFooter?.homeFooterDescription
                                Glide.with(requireActivity())
                                    .load(ApiClient.ImageURL.BASE_URL.plus(headerContenResponse?.themJson?.homeFooter?.homeFooterBackgroundImage))
                                    .into(_binding.ivHomeFooter)
                            } else {
                                _binding.clLoyality.hide()
                            }
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                headerContenResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                headerContenResponse?.message.toString()
                            )
                            startActivity(Intent(requireActivity(), ActWelCome::class.java))
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
                else -> {}
            }
        }
    }

    private fun dlgConfirm(message: String?) {
        val confirmDialogBinding = DlgConfirmBinding.inflate(layoutInflater)
        val dialog = Dialog(requireActivity())
        dialog.setContentView(confirmDialogBinding.root)
        confirmDialogBinding.tvProductDesc.text = message
        confirmDialogBinding.tvCheckOut.setOnClickListener {
            dialog.dismiss()
            openActivity(ActShoppingCart::class.java)
        }
        confirmDialogBinding.tvContinueShopping.setOnClickListener {
            dialog.dismiss()
        }

        confirmDialogBinding.tvCancel.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun dlgAlreadyCart(message: String?, id: Int?, defaultVariantId: Int?, i: Int?) {
        val builder = AlertDialog.Builder(requireActivity())
        builder.setTitle(R.string.app_name)
        builder.setMessage(R.string.product_already_again)
        builder.setPositiveButton(getString(R.string.yes)) { dialogInterface, which ->
            val cartqty = java.util.HashMap<String, String>()

            if (Utils.isLogin(requireActivity())) {
                count = 1
                cartqty["product_id"] = id.toString()
                cartqty["user_id"] =
                    SharePreference.getStringPref(
                        requireActivity(),
                        SharePreference.userId
                    ).toString()
                cartqty["variant_id"] = defaultVariantId.toString()
                cartqty["quantity_type"] = "increase"
                cartqty["theme_id"] = resources.getString(R.string.theme_id)

                cartQtyApi(cartqty, "increase")
            } else {
                val cartListData = SharePreference.getStringPref(
                    requireActivity(),
                    SharePreference.GuestCartList
                )
                val type: Type = object : TypeToken<java.util.ArrayList<ProductListItem>>() {}.type

                var cartList =
                    Gson().fromJson<java.util.ArrayList<ProductListItem>>(cartListData, type)
                var count = cartList[i!!].qty?.toInt()!!
                Log.e("Count", count.toString())
                cartList[i!!].qty = count.plus(1)

                manageOfflineData(cartList)
            }
        }
        builder.setNegativeButton(getString(R.string.no)) { dialogInterface, which ->
            builder.setCancelable(true)
        }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    private fun cartQtyApi(
        cartqty: java.util.HashMap<String, String>,
        type: String
    ) {
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .cartQty(cartqty)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val cartQtyResponse = response.body.data
                    SharePreference.setStringPref(
                        requireActivity(),
                        SharePreference.cartCount,
                        response.body.count.toString()
                    )

                    when (response.body.status) {
                        1 -> {
                            /* when (type) {
                                 "increase" -> {
                                     cartList[position].qty = count
                                     val totalQty = cartList.sumOf { it.qty!! }
                                     _binding.tvTotalQtyCount.text = totalQty.toString()
                                     cartListAdapter.notifyDataSetChanged()

                                 }
                                 "remove" -> {
                                     cartList.removeAt(position)
                                     cartListAdapter.notifyDataSetChanged()
                                     if (cartList.size > 0) {
                                         _binding.rvShoppingCart.show()
                                         _binding.tvTotalQtyCount.show()
                                         _binding.clCoupon.show()
                                         _binding.tvNoDataFound.hide()
                                         _binding.clPaymentDetails.show()
                                     } else {
                                         _binding.rvShoppingCart.hide()
                                         _binding.tvTotalQtyCount.hide()
                                         _binding.clCoupon.hide()
                                         _binding.tvNoDataFound.show()
                                         _binding.clPaymentDetails.hide()
                                     }
                                     val totalQty = cartList.sumOf { it.qty!! }
                                     _binding.tvTotalQtyCount.text = totalQty.toString()
                                 }
                                 "decrease" -> {
                                     cartList[position].qty = count
                                     val totalQty = cartList.sumOf { it.qty!! }
                                     _binding.tvTotalQtyCount.text = totalQty.toString()
                                     cartListAdapter.notifyDataSetChanged()
                                 }
                             }
                             getCartList()*/
                            Utils.dismissLoadingProgress()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                cartQtyResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                cartQtyResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(), response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
                else -> {}
            }
        }
    }

    private fun manageOfflineData(cartItemList: java.util.ArrayList<ProductListItem>) {

        SharePreference.setStringPref(
            requireActivity(),
            SharePreference.GuestCartList,
            Gson().toJson(cartItemList)
        )
        SharePreference.setStringPref(
            requireActivity(),
            SharePreference.cartCount,
            cartItemList.size.toString()
        )
    }


    override fun onResume() {
        super.onResume()

        lifecycleScope.launch {

            val currency = async {
                callCurrencyApi()
            }

            currency.await()


            lifecycleScope.launch {
                currentPage = 1
                currentPageCategorirs = 1
                currentPageBestSeller = 1
                featuredProductsList.clear()
                trendingcategoriesList.clear()

                callHeaderContentApi()
                callFeaturedProduct()
                callTrendingProduct()

                featuredProductsSubList.clear()
                categoriesProductsAdapter(featuredProductsSubList)
                callCategorysProduct()

                homeCategoriesList.clear()
                categoriesAdapter(homeCategoriesList)
                callCategories()

                bestsellersList.clear()
                bestsellerAdapter(bestsellersList)
                callBestseller()


                trendingProductsSubList.clear()
                trendingProductAdapter(trendingProductsSubList)
                callTrendingCategorysProduct()


                Log.e(
                    "Count",
                    SharePreference.getStringPref(requireActivity(), SharePreference.cartCount)
                        .toString()
                )
                _binding.tvCount.text =
                    SharePreference.getStringPref(requireActivity(), SharePreference.cartCount)
            }

        }


    }

}