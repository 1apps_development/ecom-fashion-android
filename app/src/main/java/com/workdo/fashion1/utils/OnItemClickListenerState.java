package com.workdo.fashion1.utils;

import com.workdo.fashion1.model.CountryDataItem;
import com.workdo.fashion1.model.StateListData;

public interface OnItemClickListenerState {
    void onItemClickState(StateListData item);
}
