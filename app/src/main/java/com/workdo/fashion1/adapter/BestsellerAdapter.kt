package com.workdo.fashion1.adapter

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.fashion1.R
import com.workdo.fashion1.databinding.CellBestsellerProductsBinding
import com.workdo.fashion1.databinding.CellFeaturedProductsBinding
import com.workdo.fashion1.model.FeaturedProductsSub
import com.workdo.fashion1.utils.Constants
import com.workdo.fashion1.utils.ExtensionFunctions.hide
import com.workdo.fashion1.utils.ExtensionFunctions.show
import com.workdo.fashion1.utils.SharePreference
import com.workdo.fashion1.utils.Utils

class BestsellerAdapter(
    private val context: Activity,
    private val providerList: ArrayList<FeaturedProductsSub>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<BestsellerAdapter.BestSellerViewHolder>() {

    inner class BestSellerViewHolder(private val binding: CellBestsellerProductsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: FeaturedProductsSub,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {
            var currency = SharePreference.getStringPref(context, SharePreference.currency_name).toString()

            Log.e("Cuurent", currency)
            Glide.with(context).load(data.coverImageUrl)
                .into(binding.ivBestSellers)
            binding.tvFeaturedProductName.text = data.name.toString()
            binding.tvFeaturedProductPrice.text =
                Utils.getPrice(data.finalPrice.toString())
            binding.tvcurrenytype.text = currency
            binding.tvTag.text = data.tagApi
            if (data.inWhishlist == true) {
                binding.ivWishList.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.ic_wishlist, null)
            } else {
                binding.ivWishList.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.ic_diswishlist, null)
            }

            if(Utils.isLogin(context))
            {
                binding.ivWishList.show()
            }else
            {
                binding.ivWishList.hide()

            }
            binding.ivWishList.setOnClickListener {
                itemClick(position, Constants.FavClick)
            }
            binding.ivAddtocard.setOnClickListener {
                itemClick(position, Constants.CartClick)
            }
            itemView.setOnClickListener {
                itemClick(position, Constants.ItemClick)
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BestSellerViewHolder {
        val view =
            CellBestsellerProductsBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        return BestSellerViewHolder(view)
    }

    override fun onBindViewHolder(holder: BestSellerViewHolder, position: Int) {
        holder.bind(providerList[position], context, position, itemClick)
    }

    override fun getItemCount(): Int {

        return providerList.size
    }
}