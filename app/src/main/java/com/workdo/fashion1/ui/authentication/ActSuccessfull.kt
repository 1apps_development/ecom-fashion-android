package com.workdo.fashion1.ui.authentication

import android.view.View
import com.workdo.fashion1.base.BaseActivity
import com.workdo.fashion1.databinding.ActSuccessfullBinding
import com.workdo.fashion1.ui.activity.MainActivity

class ActSuccessfull : BaseActivity() {
    private lateinit var _binding: ActSuccessfullBinding
    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActSuccessfullBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        _binding.btnGotoyourstore.setOnClickListener { openActivity(MainActivity::class.java) }
    }
}
