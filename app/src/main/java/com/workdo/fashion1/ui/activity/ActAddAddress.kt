package com.workdo.fashion1.ui.activity


import android.content.Intent
import android.util.Log
import android.view.View
import androidx.lifecycle.lifecycleScope
import com.workdo.fashion1.R
import com.workdo.fashion1.adapter.AutoCompleteCityAdapter
import com.workdo.fashion1.adapter.AutoCompleteCountryAdapter
import com.workdo.fashion1.adapter.AutoCompleteStateAdapter
import com.workdo.fashion1.api.ApiClient
import com.workdo.fashion1.base.BaseActivity
import com.workdo.fashion1.databinding.ActAddAddressBinding
import com.workdo.fashion1.model.CityListData
import com.workdo.fashion1.model.CountryDataItem
import com.workdo.fashion1.model.StateListData
import com.workdo.fashion1.remote.NetworkResponse
import com.workdo.fashion1.ui.authentication.ActWelCome
import com.workdo.fashion1.utils.*
import kotlinx.coroutines.launch


class ActAddAddress : BaseActivity(),
    OnItemClickListenerCountry, OnItemClickListenerState,OnItemClickListenerCity {
    private lateinit var _binding: ActAddAddressBinding
    private lateinit var countryAdapterauto: AutoCompleteCountryAdapter
    private lateinit var countryAdapterautoState: AutoCompleteStateAdapter
    private lateinit var countryAdapterautoCity: AutoCompleteCityAdapter

    private var countryList = ArrayList<CountryDataItem>()
    private var stateList = ArrayList<StateListData>()
    private var cityList = ArrayList<CityListData>()
    val addressTypeArray = arrayListOf<String>()
    val addressTypeArrayID = arrayListOf<String>()
    val addressTypeArrayState = arrayListOf<String>()
    var countryID = ""
    var cityID = ""
    var stateId = ""
    var checkDef = "0"
    var type = ""
    var addresstype = ""


    override fun setLayout(): View = _binding.root
    override fun initView() {
        _binding = ActAddAddressBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        addresstype = intent.getStringExtra("AddAddress").toString()
        _binding.ivBack.setOnClickListener {
            if (addresstype == "AddAddress") {
                val intent = Intent(this@ActAddAddress, ActBillingDetails::class.java)
                finish()
                startActivity(intent)
            } else {
                finish()
            }
        }
        type = intent.getStringExtra("type").toString()
        Log.e("type", type)
        _binding.radioYes.isChecked = false
        _binding.radioNo.isChecked = true
        countryID = intent.getStringExtra("country_id").toString()
        stateId = intent.getStringExtra("state_id").toString()
        _binding.edSaveas.setText(intent.getStringExtra("title"))
        _binding.edAddressName.setText(intent.getStringExtra("address"))
        _binding.autoCompleteCity.setText(intent.getStringExtra("city"))
        _binding.edPostCode.setText(intent.getStringExtra("postcode"))
        if (type == "1") {
            _binding.autoCompleteCountry.setText(intent.getStringExtra("country_name"))
            _binding.autoCompleteState.setText(intent.getStringExtra("state_name"))
        }
        _binding.radioYes.isChecked = intent.getStringExtra("default_address").toString() == "1"

        _binding.radioYes.setOnClickListener {
            checkDef = "1"

        }
        _binding.radioNo.setOnClickListener {
            checkDef = "0"
        }

        _binding.btnSaveChanges.setOnClickListener {
            when {
                _binding.edSaveas.text?.isEmpty() == true -> {
                    Utils.errorAlert(
                        this@ActAddAddress,
                        resources.getString(R.string.please_enter_save_address)
                    )
                }

                _binding.edAddressName.text?.isEmpty() == true -> {
                    Utils.errorAlert(
                        this@ActAddAddress,
                        resources.getString(R.string.please_enter_address)
                    )
                }
                _binding.autoCompleteCity.text?.isEmpty() == true -> {
                    Utils.errorAlert(
                        this@ActAddAddress,
                        resources.getString(R.string.please_enter_city)
                    )
                }
                _binding.edPostCode.text?.isEmpty() == true -> {
                    Utils.errorAlert(
                        this@ActAddAddress,
                        resources.getString(R.string.please_enter_postcode)
                    )
                }
                _binding.edPostCode.text.length<6->
                {
                    Utils.errorAlert(this@ActAddAddress, resources.getString(R.string.valid_postal_code))
                }
                _binding.autoCompleteCountry.text?.isEmpty() == true -> {
                    Utils.errorAlert(
                        this@ActAddAddress,
                        resources.getString(R.string.please_enter_country)
                    )
                }
                _binding.autoCompleteState.text?.isEmpty() == true -> {
                    Utils.errorAlert(
                        this@ActAddAddress,
                        resources.getString(R.string.please_enter_state)
                    )
                }
                else -> {

                    if (type == "1") {
                        val editAddressMap = HashMap<String, String>()
                        editAddressMap["address_id"] =
                            intent.getStringExtra("addressId").toString()
                        editAddressMap["user_id"] =
                            SharePreference.getStringPref(
                                this@ActAddAddress,
                                SharePreference.userId
                            )
                                .toString()
                        editAddressMap["title"] = _binding.edSaveas.text.toString()
                        editAddressMap["address"] = _binding.edAddressName.text.toString()
                        editAddressMap["country"] = countryID
                        editAddressMap["state"] = stateId
                        editAddressMap["city"] = _binding.autoCompleteCity.text.toString()
                        editAddressMap["postcode"] = _binding.edPostCode.text.toString()
                        editAddressMap["default_address"] = checkDef
                        editAddressMap["theme_id"] = resources.getString(R.string.theme_id)

                        callUpdateAddressApi(editAddressMap)
                    } else {
                        val addressMap = HashMap<String, String>()
                        addressMap["user_id"] =
                            SharePreference.getStringPref(
                                this@ActAddAddress,
                                SharePreference.userId
                            )
                                .toString()
                        addressMap["title"] = _binding.edSaveas.text.toString()
                        addressMap["address"] = _binding.edAddressName.text.toString()
                        addressMap["country"] = countryID
                        addressMap["state"] = stateId
                        addressMap["city"] = _binding.autoCompleteCity.text.toString()
                        addressMap["postcode"] = _binding.edPostCode.text.toString()
                        addressMap["default_address"] = checkDef
                        addressMap["theme_id"] = resources.getString(R.string.theme_id)

                        callAddressApi(addressMap)
                    }
                }
            }
        }

        getCountryApi()
    }


    //TODO address list api
    private fun callAddressApi(addressMap: HashMap<String, String>) {
        Utils.showLoadingProgress(this@ActAddAddress)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActAddAddress)
                .addAddress(addressMap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val addressResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if (addresstype == "AddAddress") {
                                val intent =
                                    Intent(this@ActAddAddress, ActBillingDetails::class.java)
                                finish()
                                startActivity(intent)
                            } else {
                                finish()
                            }
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActAddAddress,
                                addressResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActAddAddress,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActAddAddress)
                    } else {
                        Utils.errorAlert(
                            this@ActAddAddress,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActAddAddress,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActAddAddress,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //TODO Update address api
    private fun callUpdateAddressApi(editAddressMap: HashMap<String, String>) {
        Utils.showLoadingProgress(this@ActAddAddress)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActAddAddress)
                .updateAddress(editAddressMap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val addressResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            finish()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActAddAddress,
                                addressResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActAddAddress,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActAddAddress)
                    } else {
                        Utils.errorAlert(
                            this@ActAddAddress,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActAddAddress,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActAddAddress,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //TODO country list api
    private fun getCountryApi() {
        Utils.showLoadingProgress(this@ActAddAddress)
        val request=HashMap<String,String>()
        request["theme_id"]=resources.getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActAddAddress)
                .setCountryList(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val countryResponse = response.body
                    when (response.body.status) {
                        1 -> {
                            countryList = countryResponse.data!!
                            loadSpinnerCountry(countryList)
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActAddAddress,
                                countryResponse.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActAddAddress,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActAddAddress)
                    } else {
                        Utils.errorAlert(
                            this@ActAddAddress,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActAddAddress,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActAddAddress,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //TODO Country spinner
    private fun loadSpinnerCountry(countryList: ArrayList<CountryDataItem>) {
        /*for (i in 0 until countryList.size) {
            addressTypeArray.add(
                countryList[i].name.toString()
            )
        }*/

        countryAdapterauto =
            AutoCompleteCountryAdapter(
                this@ActAddAddress,
                countryList,
                OnItemClickListenerCountry {
                    onItemClick(it)
                    Log.e("StateId", it.id.toString())

                })

        _binding.autoCompleteCountry.threshold = 1
        _binding.autoCompleteCountry.setAdapter(countryAdapterauto)

        /*  ArrayAdapter(this,R.layout.row_item_list,countryList).also { adapter -> _binding.autoCompleteCountry.setAdapter(adapter)  }

          countryad(this,countryList).also { adapter ->
              _binding.autoCompleteCountry.setAdapter(adapter)
          }
          _binding.autoCompleteCountry.threshold = 1
          _binding.autoCompleteCountry.setOnItemClickListener { parent,_, position, _ ->
              val persona=parent.adapter.getItem(position) as CountryDataItem
              _binding.autoCompleteCountry.setText(persona.name.toString())
          }*/

        /*  for (i in 0 until countryList.size) {
              addressTypeArray.add(countryList[i].name.toString())
              addressTypeArrayID.add(countryList[i].id.toString())
              stateId = countryList[i].id.toString()
              Log.e("CityList", addressTypeArray.toString())
              Log.e("stateId", stateId.toString())

          }

          val arrayAdapter: ArrayAdapter<CountryDataItem> = ArrayAdapter<CountryDataItem>(
              this@ActAddAddress, R.layout.row_item_list,
              R.id.tvItemName,
              addressTypeArray as List<CountryDataItem>
          )
          _binding.autoCompleteCountry.threshold = 2
          _binding.autoCompleteCountry.setAdapter(arrayAdapter)
          _binding.autoCompleteCountry.setOnItemClickListener { adapterView, view, i, l ->
              val id = cityList[i].id
              Log.e("StateId", id.toString())
          }*/


    }

    /* //TODO country list dialog
     private fun openCountryDialog() {
         var dialog: Dialog? = null

         dialog?.dismiss()
         dialog = Dialog(this@ActAddAddress, R.style.AppCompatAlertDialogStyleBig)
         dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
         dialog.window!!.setLayout(
             WindowManager.LayoutParams.MATCH_PARENT,
             WindowManager.LayoutParams.MATCH_PARENT
         )
         dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
         dialog.setCancelable(true)
         val mView = DlgItemSelectionBinding.inflate(layoutInflater)
         val adapter = CountryAdapter(countryList) { data: CountryDataItem ->
             _binding.tvCountryName.text = data.name.toString()
             countryID = data.id.toString()
             dialog.dismiss()
             getStateApi(countryID)
         }

         mView.rvItemList.adapter = adapter
         mView.rvItemList.layoutManager = LinearLayoutManager(this@ActAddAddress)
         mView.rvItemList.itemAnimator = DefaultItemAnimator()
         mView.rvItemList.isNestedScrollingEnabled = true


         dialog.setContentView(mView.root)
         dialog.show()
     }*/

    //TODO state list api
    private fun getStateApi(countryID: String) {
        Utils.showLoadingProgress(this@ActAddAddress)
        val stateListMap = HashMap<String, String>()
        stateListMap["country_id"] = countryID
        stateListMap["theme_id"] = resources.getString(R.string.theme_id)

        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActAddAddress)
                .setStateList(stateListMap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val stateListResponse = response.body
                    when (response.body.status) {
                        1 -> {
                            stateList = stateListResponse.data!!

                            loadSpinnerState(stateList)
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActAddAddress,
                                stateListResponse.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActAddAddress,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActAddAddress)
                    } else {
                        Utils.errorAlert(
                            this@ActAddAddress,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActAddAddress,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActAddAddress,
                        "Something went wrong"
                    )
                }
            }
        }
    }


    //TODO state list api
    private fun getCityApi(stateId: String) {
        Utils.showLoadingProgress(this@ActAddAddress)
        val stateListMap = HashMap<String, String>()
        stateListMap["state_id"] = stateId
        stateListMap["theme_id"] = resources.getString(R.string.theme_id)

        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActAddAddress)
                .setCityList(stateListMap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val stateListResponse = response.body
                    when (response.body.status) {
                        1 -> {
                            cityList = stateListResponse.data!!

                            loadCityAdapter(cityList)
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActAddAddress,
                                stateListResponse.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActAddAddress,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActAddAddress)
                    } else {
                        Utils.errorAlert(
                            this@ActAddAddress,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActAddAddress,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActAddAddress,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    private fun loadCityAdapter(cityList: ArrayList<CityListData>) {
    /*    for (element in cityList) {
            addressTypeArraycity.add(element.name.toString())
            Log.e("CityList", addressTypeArraycity.toString())

        }

        val arrayAdapter: ArrayAdapter<CityListData> = ArrayAdapter<CityListData>(
            this@ActAddAddress, R.layout.row_item_list,
            R.id.tvItemName,
            addressTypeArraycity as List<CityListData>
        )
        _binding.autoCompleteCity.threshold = 2
        _binding.autoCompleteCity.setAdapter(arrayAdapter)*/

        countryAdapterautoCity =
            AutoCompleteCityAdapter(
                this@ActAddAddress,
                cityList
            ) {
                onItemClickCity(it)
                Log.e("StateId", it.id.toString())

            }

        _binding.autoCompleteCity.threshold = 1
        _binding.autoCompleteCity.setAdapter(countryAdapterautoCity)
    }

    /*  //TODO state list dialog
      private fun openStateListDialog() {
          var dialog: Dialog? = null
          dialog?.dismiss()
          dialog = Dialog(this@ActAddAddress, R.style.AppCompatAlertDialogStyleBig)
          dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
          dialog.window!!.setLayout(
              WindowManager.LayoutParams.MATCH_PARENT,
              WindowManager.LayoutParams.MATCH_PARENT
          )
          dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
          dialog.setCancelable(true)
          val mView = DlgItemSelectionBinding.inflate(layoutInflater)
          val adapter = StateListAdapter(stateList) { data: StateListData ->
              _binding.tvStateName.text = data.name.toString()
              stateId = data.id.toString()
              dialog.dismiss()
              getCityApi(stateId)
          }

          mView.rvItemList.adapter = adapter
          mView.rvItemList.layoutManager = LinearLayoutManager(this@ActAddAddress)
          mView.rvItemList.itemAnimator = DefaultItemAnimator()
          mView.rvItemList.isNestedScrollingEnabled = true
          dialog.setContentView(mView.root)
          val layoutParams = dialog.window!!.attributes
          layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT
          dialog.window!!.attributes = layoutParams
          dialog.show()
      }
  */
    //TODO State list spinner
    private fun loadSpinnerState(stateList: ArrayList<StateListData>) {
        /*addressTypeArrayState.clear()
        for (i in 0 until stateList.size) {
            addressTypeArrayState.add(
                stateList[i].name.toString()
            )
        }*/

        countryAdapterautoState =
            AutoCompleteStateAdapter(
                this@ActAddAddress,
                stateList
            ) {
                onItemClickState(it)
                Log.e("StateId", it.id.toString())

            }

        _binding.autoCompleteState.threshold = 1
        _binding.autoCompleteState.setAdapter(countryAdapterautoState)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (addresstype == "AddAddress") {
            val intent = Intent(this@ActAddAddress, ActBillingDetails::class.java)
            finish()
            startActivity(intent)
        } else {
            finish()
        }
    }

    override fun onItemClick(item: CountryDataItem?) {
        Log.e("StateId", item?.id.toString())
        _binding.autoCompleteState.text.clear()
        _binding.autoCompleteCity.text.clear()
        _binding.autoCompleteCountry.setText(item?.name)
        countryID=item?.id.toString()
        stateId = item?.id.toString()
        getStateApi(item?.id.toString())
    }

    override fun onItemClickState(item: StateListData?) {
        _binding.autoCompleteState.setText(item?.name)
        _binding.autoCompleteCity.text.clear()
        stateId = item?.id.toString()
        cityID = item?.id.toString()
        getCityApi(item?.id.toString())
    }

    override fun onItemClickCity(item: CityListData?) {
        _binding.autoCompleteCity.setText(item?.name)
        cityID=item?.id.toString()
    }
}
