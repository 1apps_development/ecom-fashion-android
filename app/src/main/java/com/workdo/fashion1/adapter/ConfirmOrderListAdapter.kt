package com.workdo.fashion1.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.fashion1.R
import com.workdo.fashion1.api.ApiClient
import com.workdo.fashion1.databinding.CellOrderdetailslistBinding
import com.workdo.fashion1.model.ProductItem
import com.workdo.fashion1.model.ProductListItem
import com.workdo.fashion1.utils.ExtensionFunctions.hide
import com.workdo.fashion1.utils.SharePreference
import com.workdo.fashion1.utils.Utils

class ConfirmOrderListAdapter(
    private val context: Activity,
    private val orderlist: ArrayList<ProductListItem>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<ConfirmOrderListAdapter.WishlistViewHolder>() {
    var currency = SharePreference.getStringPref(context, SharePreference.currency)

    inner class WishlistViewHolder(private val binding: CellOrderdetailslistBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: ProductListItem,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {

            binding.tvWishlist.text = data.name
            binding.tvVariantName.text = data.variantName
            binding.tvPrice.text = currency.plus(data.finalPrice.toString().let { Utils.getPrice(it) })
            Glide.with(context).load(ApiClient.ImageURL.BASE_URL.plus(data.image)).into(binding.ivWishList)
            binding.tvQty.text=context.getString(R.string.qty).plus(" ").plus(data.qty)

            if(data.variantName.isNullOrEmpty()||data.variantName=="null")
            {
                binding.tvVariantName.hide()
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WishlistViewHolder {
        val view =
            CellOrderdetailslistBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return WishlistViewHolder(view)
    }

    override fun onBindViewHolder(holder: WishlistViewHolder, position: Int) {
        holder.bind(orderlist[position], context, position, itemClick)
    }

    override fun getItemCount(): Int {

        return orderlist.size
    }
}