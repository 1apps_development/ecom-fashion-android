package com.workdo.fashion1.ui.option

import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.workdo.fashion1.R
import com.workdo.fashion1.adapter.MenuListAdapter
import com.workdo.fashion1.api.ApiClient
import com.workdo.fashion1.base.BaseActivity
import com.workdo.fashion1.databinding.ActMenuBinding
import com.workdo.fashion1.model.DataItems
import com.workdo.fashion1.remote.NetworkResponse
import com.workdo.fashion1.ui.authentication.ActWelCome
import com.workdo.fashion1.utils.ExtensionFunctions.hide
import com.workdo.fashion1.utils.ExtensionFunctions.show
import com.workdo.fashion1.utils.SharePreference
import com.workdo.fashion1.utils.Utils
import kotlinx.coroutines.launch

class ActMenu : BaseActivity() {
    private lateinit var _binding: ActMenuBinding
    private var managerBestsellers: LinearLayoutManager? = null
    private var menuList = ArrayList<DataItems>()
    private lateinit var menuListAdapter: MenuListAdapter

    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActMenuBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        if(!Utils.isLogin(this@ActMenu))
        {
            _binding.tvLogin.show()
        }else
        {
            _binding.tvLogin.hide()

        }


        _binding.ivClose.setOnClickListener { finish() }

        _binding.tvLogin.setOnClickListener {
            val intent = Intent(this@ActMenu, ActWelCome::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)
            finish()
        }
        managerBestsellers = LinearLayoutManager(this@ActMenu)
        _binding.ivInstragram.setOnClickListener {
            val contactUs=
                SharePreference.getStringPref(this@ActMenu, SharePreference.insta).toString()
            val uri: Uri =
                Uri.parse(contactUs)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            intent.addCategory(Intent.CATEGORY_BROWSABLE)

            startActivity(intent)
        }
        _binding.ivMessage.setOnClickListener {
            val contactUs=
                SharePreference.getStringPref(this@ActMenu, SharePreference.messanger).toString()
            val uri: Uri =
                Uri.parse(contactUs)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            intent.addCategory(Intent.CATEGORY_BROWSABLE)

            startActivity(intent)
        }
        _binding.ivYoutube.setOnClickListener {
            val contactUs=
                SharePreference.getStringPref(this@ActMenu, SharePreference.youtube).toString()
            val uri: Uri =
                Uri.parse(contactUs)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            intent.addCategory(Intent.CATEGORY_BROWSABLE)

            startActivity(intent)
        }
        _binding.ivTwitter.setOnClickListener {
            val contactUs=
                SharePreference.getStringPref(this@ActMenu, SharePreference.twitter).toString()
            val uri: Uri =
                Uri.parse(contactUs)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            intent.addCategory(Intent.CATEGORY_BROWSABLE)

            startActivity(intent)
        }
    }

    //Adapter set Best seller
    private fun menuAdapter(menuList: ArrayList<DataItems>) {
        _binding.rvMenulist.layoutManager = managerBestsellers
        menuListAdapter =
            MenuListAdapter(this@ActMenu, menuList) { i: Int, s: String ->
            }
        _binding.rvMenulist.adapter = menuListAdapter
    }

    private fun callNavigation() {
        Utils.showLoadingProgress(this@ActMenu)
        val request = HashMap<String, String>()
        request["theme_id"]=resources.getString(R.string.theme_id)

        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActMenu)
                .navigation(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val stateListResponse = response.body
                    when (response.body.status) {
                        1 -> {
                            if ((stateListResponse.data?.size ?: 0) > 0) {
                                _binding.rvMenulist.show()
                                stateListResponse.data?.let { menuList.addAll(it) }
                            } else {
                                _binding.rvMenulist.hide()
                            }
                            menuListAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActMenu,
                                stateListResponse.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActMenu,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActMenu)
                    } else {
                        Utils.errorAlert(
                            this@ActMenu,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActMenu,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActMenu,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        menuList.clear()
        menuAdapter(menuList)
        callNavigation()
    }
}