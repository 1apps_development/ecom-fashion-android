package com.workdo.fashion1.ui.activity

import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.util.Log
import android.view.View
import androidx.core.view.ViewCompat
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.workdo.fashion1.R
import com.workdo.fashion1.adapter.ProductCategoryAdapter
import com.workdo.fashion1.api.ApiClient
import com.workdo.fashion1.base.BaseActivity
import com.workdo.fashion1.databinding.ActBestsellersBinding
import com.workdo.fashion1.databinding.DlgConfirmBinding
import com.workdo.fashion1.model.FeaturedProductsSub
import com.workdo.fashion1.model.ProductListItem
import com.workdo.fashion1.remote.NetworkResponse
import com.workdo.fashion1.ui.authentication.ActWelCome
import com.workdo.fashion1.ui.option.ActFilter
import com.workdo.fashion1.utils.Constants
import com.workdo.fashion1.utils.ExtensionFunctions.hide
import com.workdo.fashion1.utils.ExtensionFunctions.show
import com.workdo.fashion1.utils.PaginationScrollListener
import com.workdo.fashion1.utils.SharePreference
import com.workdo.fashion1.utils.Utils
import kotlinx.coroutines.launch
import java.lang.reflect.Type

class ActBestsellers : BaseActivity() {
    private lateinit var _binding: ActBestsellersBinding
    private var managerBestsellers: GridLayoutManager? = null
    private var bestsellersList = ArrayList<FeaturedProductsSub>()
    private lateinit var bestSellersAdapter: ProductCategoryAdapter
    internal var isLoadingBestSeller = false
    internal var isLastPageBestSeller = false
    private var currentPageBestSeller = 1
    private var total_pagesBestSeller: Int = 0
    var count = 1

    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActBestsellersBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        _binding.ivFilter.setOnClickListener { openActivity(ActFilter::class.java) }
        _binding.icBack.setOnClickListener { finish() }
        managerBestsellers =
            GridLayoutManager(this@ActBestsellers, 2, GridLayoutManager.VERTICAL, false)
        //nestedScrollViewPagination()
       // productBannerApi()
    }
/*
    private fun productBannerApi() {
        Utils.showLoadingProgress(this@ActBestsellers)
        val productBanner = HashMap<String, String>()
        productBanner["theme_id"] = "style"
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActBestsellers)
                .productBanner(productBanner)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val bannersResponse = response.body.data?.themJson?.productsHeaderBanner
                    when (response.body.status) {
                        1 -> {
                            Glide.with(this@ActBestsellers).load(
                                ApiClient.ImageURL.BASE_URL.plus(
                                   bannersResponse?.productsHeaderBanner
                                )
                            )
                                .into(_binding.ivProductBanner)
                            _binding.tvmess.text =
                                bannersResponse?.productsHeaderBannerTitleText.toString()

                        }
                        0 -> {

                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActBestsellers)
                    } else {
                        Utils.errorAlert(
                            this@ActBestsellers,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActBestsellers,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActBestsellers,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //TODO bestseller api c
    private fun callBestseller() {
        Utils.showLoadingProgress(this@ActBestsellers)
        lifecycleScope.launch {
            val response =
                if (SharePreference.getStringPref(this@ActBestsellers, SharePreference.token)
                        ?.isEmpty() == true
                ) {
                    ApiClient.getClient(this@ActBestsellers)
                        .setBestSellerGuest(currentPageBestSeller.toString())
                } else {
                    ApiClient.getClient(this@ActBestsellers)
                        .setBestSeller(currentPageBestSeller.toString())
                }
            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val bestsellerResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                _binding.rvBestsellers.show()
                                _binding.vieww.hide()
                                currentPageBestSeller =
                                    bestsellerResponse?.currentPage!!.toInt()
                                total_pagesBestSeller =
                                    bestsellerResponse.lastPage!!.toInt()
                                bestsellerResponse.data?.let {
                                    bestsellersList.addAll(it)
                                }
                                if (currentPageBestSeller >= total_pagesBestSeller) {
                                    isLastPageBestSeller = true
                                }
                                isLoadingBestSeller = false
                            } else {
                                _binding.rvBestsellers.hide()
                                _binding.vieww.show()

                            }
                            bestSellersAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActBestsellers,
                                bestsellerResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActBestsellers,
                                bestsellerResponse?.data?.get(0)?.message.toString()
                            )
                            Utils.setInvalidToekn(this@ActBestsellers)
                        }
                    }
                    if (bestsellerResponse?.to == null) {

                        _binding.tvFoundproduct.text = "Found ".plus("0").plus(" ").plus("product")
                    } else {
                        _binding.tvFoundproduct.text = "Found ".plus(
                            bestsellerResponse.to
                        ).plus(" ").plus("product")
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActBestsellers)
                    } else {
                        Utils.errorAlert(
                            this@ActBestsellers,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActBestsellers,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActBestsellers,
                        getString(R.string.something_went_wrong)
                    )
                }
            }
        }
    }


    //TODO Adapter set Best seller
    private fun bestsellerAdapter(bestsellersList: ArrayList<FeaturedProductsSub>) {
        _binding.rvBestsellers.layoutManager = managerBestsellers
        bestSellersAdapter =
            ProductCategoryAdapter(this@ActBestsellers, bestsellersList) { i: Int, s: String ->
                if (s == Constants.FavClick) {
                    if (SharePreference.getBooleanPref(
                            this@ActBestsellers,
                            SharePreference.isLogin
                        )
                    ) {
                        Log.e("FavClick", "FavClick")
                        if (bestsellersList[i].inWhishlist == false) {
                            callWishlist(
                                "add", bestsellersList[i].id, "BestsellersList", i
                            )
                        } else {
                            callWishlist(
                                "remove",
                                bestsellersList[i].id,
                                "BestsellersList",
                                i
                            )
                        }
                    } else {
                        openActivity(ActWelCome::class.java)
                        finish()
                    }
                } else if (s == Constants.CartClick) {
                    if (!Utils.isLogin(this@ActBestsellers)) {
                        guestUserAddToCart(bestsellersList[i], bestsellersList[i].id)
                    } else {
                        val addtocart = HashMap<String, String>()
                        addtocart["user_id"] =
                            SharePreference.getStringPref(
                                this@ActBestsellers,
                                SharePreference.userId
                            )
                                .toString()
                        addtocart["product_id"] = bestsellersList[i].id.toString()
                        addtocart["variant_id"] = bestsellersList[i].defaultVariantId.toString()
                        addtocart["qty"] = "1"
                        addtocartApi(
                            addtocart,
                            bestsellersList[i].id,
                            bestsellersList[i].defaultVariantId, i
                        )
                    }

                } else if (s == Constants.ItemClick) {
                    val intent = Intent(this@ActBestsellers, ActProductDetails::class.java)
                    intent.putExtra(Constants.ProductId, bestsellersList[i].id.toString())
                    startActivity(intent)
                }
            }
        _binding.rvBestsellers.adapter = bestSellersAdapter
        _binding.rvBestsellers.isNestedScrollingEnabled = false
    }


    private fun guestUserAddToCart(data: FeaturedProductsSub, id: Int?) {
        val cartData = cartData(data)
        Log.e("cartData2", Gson().toJson(cartData))
        val cartListData = SharePreference.getStringPref(
            this@ActBestsellers,
            SharePreference.GuestCartList
        )
        val type: Type = object : TypeToken<ArrayList<ProductListItem>>() {}.type
        var cartList = Gson().fromJson<ArrayList<ProductListItem>>(cartListData, type)
        Log.e("cartListBefore2", Gson().toJson(cartList))
        if (cartList == null) {
            val cartDataList = ArrayList<ProductListItem>()
            cartDataList.add(cartData)
            SharePreference.setStringPref(
                this@ActBestsellers,
                SharePreference.GuestCartList,
                Gson().toJson(cartDataList)
            )
            SharePreference.setStringPref(
                this@ActBestsellers,
                SharePreference.cartCount,
                cartDataList.size.toString()
            )
        } else if (isProductAlreadyAdded(cartList, data.id.toString())) {
            //Utils.errorAlert(this@ActBestsellers, "item already added into cart")
            for (i in 0 until cartList.size) {
                if (cartList[i].productId?.toInt() == id) {
                    val position = cartList[i]
                    Log.e("SelectId", position.toString())
                    dlgAlreadyCart(data.message, data.id, data.defaultVariantId, i)
                }
            }

        } else {
            dlgConfirm(data.name.plus(" ").plus("add successfully."))
            cartList.add(cartData)
            SharePreference.setStringPref(
                this@ActBestsellers,
                SharePreference.GuestCartList,
                Gson().toJson(cartList)
            )
            SharePreference.setStringPref(
                this@ActBestsellers,
                SharePreference.cartCount,
                cartList.size.toString()
            )
        }
    }

    private fun cartData(data: FeaturedProductsSub): ProductListItem {
        return ProductListItem(
            data.coverImagePath.toString(),
            data.defaultVariantId.toString(),
            data.finalPrice.toString(),
            data.discountPrice.toString(),
            data.id.toString(),
            1,
            data.name.toString(),
            "",
            "",
            data.variantName.toString(),
            data.originalPrice.toString()
        )
    }

    private fun isProductAlreadyAdded(cartList: ArrayList<ProductListItem>, id: String): Boolean {
        return cartList.filter { it.productId == id.toString() }.size == 1
    }

    private fun nestedScrollViewPagination() {
        _binding.rvBestsellers.isNestedScrollingEnabled = false
        _binding.scrollView.viewTreeObserver?.addOnScrollChangedListener {
            val view = _binding.scrollView.getChildAt(_binding.scrollView.childCount - 1)

            val diff = view.bottom - (_binding.scrollView.height + _binding.scrollView.scrollY)

            if (diff == 0 && !isLoadingBestSeller && !isLastPageBestSeller) {
                currentPageBestSeller++
                callBestseller()
            }
        }
    }

    //TODO wishlist api
    private fun callWishlist(type: String, id: Int?, itemType: String, position: Int) {
        Utils.showLoadingProgress(this@ActBestsellers)
        val wishlist = HashMap<String, String>()
        wishlist["user_id"] =
            SharePreference.getStringPref(this@ActBestsellers, SharePreference.userId).toString()
        wishlist["product_id"] = id.toString()
        wishlist["wishlist_type"] = type
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActBestsellers)
                .setWishlist(wishlist)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val wishlistResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if (type == "add") {
                                if (itemType == "BestsellersList") {
                                    bestsellersList[position].inWhishlist = true
                                    bestSellersAdapter.notifyItemChanged(position)
                                }
                            } else {
                                if (itemType == "BestsellersList") {
                                    bestsellersList[position].inWhishlist = false
                                    bestSellersAdapter.notifyItemChanged(position)
                                }
                            }
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActBestsellers,
                                wishlistResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActBestsellers,
                                wishlistResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                            this@ActBestsellers.finish()
                            this@ActBestsellers.finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActBestsellers)
                    } else {
                        Utils.errorAlert(
                            this@ActBestsellers,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActBestsellers,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActBestsellers,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //TODO add to cart api
    private fun addtocartApi(
        addtocart: HashMap<String, String>,
        id: Int?,
        defaultVariantId: Int?,
        i: Int?
    ) {
        Utils.showLoadingProgress(this@ActBestsellers)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActBestsellers)
                .addtocart(addtocart)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val addtocart = response.body.data
                    when (response.body.status) {
                        1 -> {
                            dlgConfirm(response.body.data?.message)
                            SharePreference.setStringPref(
                                this@ActBestsellers,
                                SharePreference.cartCount,
                                addtocart?.count.toString()
                            )
                            // setCount(addtocart?.count)
                        }
                        0 -> {
                            dlgAlreadyCart(response.body.data?.message, id, defaultVariantId, i)
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActBestsellers,
                                addtocart?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                            this@ActBestsellers.finish()
                            this@ActBestsellers.finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActBestsellers)
                    } else {
                        Utils.errorAlert(
                            this@ActBestsellers,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActBestsellers,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActBestsellers,
                        "Something went wrong"
                    )
                }

            }
        }
    }


    private fun dlgConfirm(message: String?) {
        val confirmDialogBinding = DlgConfirmBinding.inflate(layoutInflater)
        val dialog = Dialog(this@ActBestsellers)
        dialog.setContentView(confirmDialogBinding.root)
        confirmDialogBinding.tvProductDesc.text = message
        confirmDialogBinding.tvCheckOut.setOnClickListener {
            dialog.dismiss()
            openActivity(ActShoppingCart::class.java)
        }
        confirmDialogBinding.tvContinueShopping.setOnClickListener {
            dialog.dismiss()
        }

        confirmDialogBinding.tvCancel.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun dlgAlreadyCart(message: String?, id: Int?, defaultVariantId: Int?, i: Int?) {
        val builder = AlertDialog.Builder(this@ActBestsellers)
        builder.setTitle(R.string.app_name)
        builder.setMessage(R.string.product_already_again)
        builder.setPositiveButton(getString(R.string.yes)) { dialogInterface, which ->
            val cartqty = java.util.HashMap<String, String>()

            if (Utils.isLogin(this@ActBestsellers)) {
                count = 1
                cartqty["product_id"] = id.toString()
                cartqty["user_id"] =
                    SharePreference.getStringPref(
                        this@ActBestsellers,
                        SharePreference.userId
                    ).toString()
                cartqty["variant_id"] = defaultVariantId.toString()
                cartqty["quantity_type"] = "increase"
                cartQtyApi(cartqty, "increase")
            } else {
                val cartListData = SharePreference.getStringPref(
                    this@ActBestsellers,
                    SharePreference.GuestCartList
                )
                val type: Type = object : TypeToken<java.util.ArrayList<ProductListItem>>() {}.type

                var cartList =
                    Gson().fromJson<java.util.ArrayList<ProductListItem>>(cartListData, type)
                var count = cartList[i!!].qty?.toInt()!!
                Log.e("Count", count.toString())
                cartList[i!!].qty = count.plus(1)

                manageOfflineData(cartList)
            }
        }
        builder.setNegativeButton(getString(R.string.no)) { dialogInterface, which ->
            builder.setCancelable(true)
        }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    private fun cartQtyApi(
        cartqty: java.util.HashMap<String, String>,
        type: String
    ) {
        Utils.showLoadingProgress(this@ActBestsellers)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActBestsellers)
                .cartQty(cartqty)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val cartQtyResponse = response.body.data
                    SharePreference.setStringPref(
                        this@ActBestsellers,
                        SharePreference.cartCount,
                        response.body.count.toString()
                    )

                    when (response.body.status) {
                        1 -> {
                            *//* when (type) {
                                 "increase" -> {
                                     cartList[position].qty = count
                                     val totalQty = cartList.sumOf { it.qty!! }
                                     _binding.tvTotalQtyCount.text = totalQty.toString()
                                     cartListAdapter.notifyDataSetChanged()

                                 }
                                 "remove" -> {
                                     cartList.removeAt(position)
                                     cartListAdapter.notifyDataSetChanged()
                                     if (cartList.size > 0) {
                                         _binding.rvShoppingCart.show()
                                         _binding.tvTotalQtyCount.show()
                                         _binding.clCoupon.show()
                                         _binding.tvNoDataFound.hide()
                                         _binding.clPaymentDetails.show()
                                     } else {
                                         _binding.rvShoppingCart.hide()
                                         _binding.tvTotalQtyCount.hide()
                                         _binding.clCoupon.hide()
                                         _binding.tvNoDataFound.show()
                                         _binding.clPaymentDetails.hide()
                                     }
                                     val totalQty = cartList.sumOf { it.qty!! }
                                     _binding.tvTotalQtyCount.text = totalQty.toString()
                                 }
                                 "decrease" -> {
                                     cartList[position].qty = count
                                     val totalQty = cartList.sumOf { it.qty!! }
                                     _binding.tvTotalQtyCount.text = totalQty.toString()
                                     cartListAdapter.notifyDataSetChanged()
                                 }
                             }
                             getCartList()*//*
                            Utils.dismissLoadingProgress()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActBestsellers,
                                cartQtyResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActBestsellers,
                                cartQtyResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActBestsellers)
                    } else {
                        Utils.errorAlert(
                            this@ActBestsellers, response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActBestsellers,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActBestsellers, "Something went wrong")
                }
            }
        }
    }

    private fun manageOfflineData(cartItemList: java.util.ArrayList<ProductListItem>) {
        SharePreference.setStringPref(
            this@ActBestsellers,
            SharePreference.GuestCartList,
            Gson().toJson(cartItemList)
        )
        SharePreference.setStringPref(
            this@ActBestsellers,
            SharePreference.cartCount,
            cartItemList.size.toString()
        )
    }

    override fun onResume() {
        super.onResume()
        currentPageBestSeller = 1
        bestsellersList.clear()
        bestsellerAdapter(bestsellersList)
        callBestseller()
    }

    *//*private fun paginationBestSeller() {
        val paginationListener = object : PaginationScrollListener(managerBestsellers) {
            override fun isLastPage(): Boolean {
                return isLastPageBestSeller
            }

            override fun isLoading(): Boolean {
                return isLoadingBestSeller
            }

            override fun loadMoreItems() {
                isLoadingBestSeller = true
                currentPageBestSeller++
                callBestseller()
            }
        }
        _binding.rvBestsellers.addOnScrollListener(paginationListener)
    }*/
}