package com.workdo.fashion1.utils;

import com.workdo.fashion1.model.CountryDataItem;

public interface OnItemClickListenerGuestCountry {
    void onItemClickGuest(CountryDataItem item);
}
