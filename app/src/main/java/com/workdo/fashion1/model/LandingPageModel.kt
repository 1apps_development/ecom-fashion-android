package com.workdo.fashion1.model

import com.google.gson.annotations.SerializedName

data class LandingPageModel(

	@field:SerializedName("data")
	val data: LandingPageData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class LandingPageData(

	@field:SerializedName("them_json")
	val themJson: ThemJson? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("loyality_section")
	val loyalitySection: String? = null
)

data class HomeBestsellersFeaturedCollection(

	@field:SerializedName("home-bestsellers-featured-collection-title-text")
	val homeBestsellersFeaturedCollectionTitleText: String? = null,

	@field:SerializedName("home-bestsellers-featured-collection-main-text")
	val homeBestsellersFeaturedCollectionMainText: String? = null
)

data class ProductsHeaderBanner(

	@field:SerializedName("products-header-banner-title-text")
	val productsHeaderBannerTitleText: String? = null,

	@field:SerializedName("products-header-banner")
	val productsHeaderBanner: String? = null
)

data class HomeCategoriesFeaturedCollection(

	@field:SerializedName("home-categories-featured-collection-title-text")
	val homeCategoriesFeaturedCollectionTitleText: String? = null,

	@field:SerializedName("home-categories-featured-collection-category")
	val homeCategoriesFeaturedCollectionCategory: String? = null,

	@field:SerializedName("home-categories-featured-collection-category-button-text")
	val homeCategoriesFeaturedCollectionCategoryButtonText: String? = null,

	@field:SerializedName("home-categories-featured-collection-top-text")
	val homeCategoriesFeaturedCollectionTopText: String? = null,

	@field:SerializedName("home-categories-featured-collection-description")
	val homeCategoriesFeaturedCollectionDescription: String? = null
)

data class ThemJson(

	@field:SerializedName("home-categories-featured-collection")
	val homeCategoriesFeaturedCollection: HomeCategoriesFeaturedCollection? = null,

	@field:SerializedName("home-featured-product")
	val homeFeaturedProduct: HomeFeaturedProduct? = null,

	@field:SerializedName("home-trending-collection")
	val homeTrendingCollection: HomeTrendingCollection? = null,

	@field:SerializedName("home-bestsellers-featured-collection")
	val homeBestsellersFeaturedCollection: HomeBestsellersFeaturedCollection? = null,

	@field:SerializedName("home-footer")
	val homeFooter: HomeFooter? = null,

	@field:SerializedName("home-search")
	val homeSearch: HomeSearch? = null,

	@field:SerializedName("products-header-banner")
	val productsHeaderBanner: ProductsHeaderBanner? = null,

	@field:SerializedName("home-header")
	val homeHeader: HomeHeader? = null
)

data class HomeFooter(

	@field:SerializedName("home-footer-description")
	val homeFooterDescription: String? = null,

	@field:SerializedName("home-footer-background-image")
	val homeFooterBackgroundImage: String? = null,

	@field:SerializedName("home-footer-title-text")
	val homeFooterTitleText: String? = null
)

data class HomeSearch(

	@field:SerializedName("home-search-search-product")
	val homeSearchSearchProduct: String? = null,

	@field:SerializedName("home-search-search-product-ie")
	val homeSearchSearchProductIe: String? = null
)

data class HomeHeader(

	@field:SerializedName("home-header-title-text")
	val homeHeaderTitleText: String? = null,

	@field:SerializedName("home-header-sub-text")
	val homeHeaderSubText: String? = null,

	@field:SerializedName("home-header-background-image")
	val homeHeaderBackgroundImage: String? = null
)

data class HomeFeaturedProduct(

	@field:SerializedName("home-featured-product-tile-text")
	val homeFeaturedProductTileText: String? = null,

	@field:SerializedName("home-featured-product-sub-text")
	val homeFeaturedProductSubText: String? = null
)

data class HomeTrendingCollection(

	@field:SerializedName("home-trending-collection-title-text")
	val homeTrendingCollectionTitleText: String? = null,

	@field:SerializedName("home-trending-collection-main-text")
	val homeTrendingCollectionMainText: String? = null
)


