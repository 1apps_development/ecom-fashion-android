package com.workdo.fashion1.ui.activity

import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import com.workdo.fashion1.R
import com.workdo.fashion1.adapter.OrderHistoryAdapter
import com.workdo.fashion1.adapter.WishListAdapter
import com.workdo.fashion1.base.BaseActivity
import com.workdo.fashion1.databinding.ActWishlistBinding
import com.workdo.fashion1.model.CategoriesModel

class ActWishlist : BaseActivity() {
    private lateinit var _binding: ActWishlistBinding

    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActWishlistBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        _binding.ivBack.setOnClickListener { finish() }
    }
}