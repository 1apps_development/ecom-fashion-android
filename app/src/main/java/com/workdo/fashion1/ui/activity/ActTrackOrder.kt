package com.workdo.fashion1.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.workdo.fashion1.R
import com.workdo.fashion1.base.BaseActivity
import com.workdo.fashion1.databinding.ActTrackOrderBinding

class ActTrackOrder : BaseActivity() {
    private lateinit var _binding: ActTrackOrderBinding
    var trackOrderStatus = ""

    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActTrackOrderBinding.inflate(layoutInflater)
        trackOrderStatus = intent.getStringExtra("trackOrderStatus").toString()
        inti()
    }

    private fun inti() {
        _binding.ivBack.setOnClickListener { finish() }
        if (trackOrderStatus=="0"){
            _binding.ivOrderConfirm.setImageResource(R.drawable.ic_option)
            _binding.ivOrderDelivered.setImageResource(R.drawable.ic_unoption)
            _binding.view.setBackgroundColor(getColor(R.color.lightpink))
        }else if (trackOrderStatus=="1"){
            _binding.ivOrderConfirm.setImageResource(R.drawable.ic_option)
            _binding.ivOrderDelivered.setImageResource(R.drawable.ic_option)
            _binding.view.setBackgroundColor(getColor(R.color.lightpink))

        }

    }

}