package com.workdo.fashion1.utils;

import com.workdo.fashion1.model.CountryDataItem;

public interface OnItemClickListenerCountry {
    void onItemClick(CountryDataItem item);
}
