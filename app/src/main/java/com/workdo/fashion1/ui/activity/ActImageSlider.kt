package com.workdo.fashion1.ui.activity

import android.util.Log
import android.view.View
import com.denzcoskun.imageslider.models.SlideModel
import com.workdo.fashion1.api.ApiClient
import com.workdo.fashion1.base.BaseActivity
import com.workdo.fashion1.databinding.ActImageSliderBinding
import com.workdo.fashion1.model.ProductImageItem

class ActImageSlider : BaseActivity() {
    private lateinit var imageSliderBinding: ActImageSliderBinding
    var imgList: ArrayList<ProductImageItem>? = null

    override fun setLayout(): View = imageSliderBinding.root

    override fun initView() {
        imageSliderBinding = ActImageSliderBinding.inflate(layoutInflater)
        imgList = intent.getParcelableArrayListExtra("imageList")
        Log.e("ImageList",imgList.toString())
        val imageList = ArrayList<SlideModel>()
        for (i in 0 until imgList?.size!!) {
            val slideModel = SlideModel(ApiClient.ImageURL.BASE_URL.plus(imgList!![i].imagePath))
            imageList.add(slideModel)
        }
        imageSliderBinding.imageSlider.setImageList(imageList)
        imageSliderBinding.ivCancle.setOnClickListener {
            finish()
        }
    }
}