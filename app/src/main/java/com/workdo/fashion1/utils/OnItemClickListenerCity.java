package com.workdo.fashion1.utils;

import com.workdo.fashion1.model.CityListData;
import com.workdo.fashion1.model.StateListData;

public interface OnItemClickListenerCity {
    void onItemClickCity(CityListData item);
}
