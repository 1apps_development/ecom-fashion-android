package com.workdo.fashion1.ui.option

import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import com.workdo.fashion1.R
import com.workdo.fashion1.base.BaseActivity
import com.workdo.fashion1.databinding.ActCartBinding
import com.workdo.fashion1.model.CategoriesModel
import com.workdo.fashion1.ui.activity.ActShoppingCart

class ActCart : BaseActivity() {
    private lateinit var _binding: ActCartBinding
    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActCartBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        _binding.ivBack.setOnClickListener { finish() }
        _binding.btnAllProduct.setOnClickListener { openActivity(ActShoppingCart::class.java) }
    }
}