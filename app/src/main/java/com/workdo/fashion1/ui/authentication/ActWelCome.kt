package com.workdo.fashion1.ui.authentication

import android.content.Intent
import android.view.View
import androidx.lifecycle.lifecycleScope
import com.workdo.fashion1.R
import com.workdo.fashion1.api.ApiClient
import com.workdo.fashion1.base.BaseActivity
import com.workdo.fashion1.databinding.ActWelComeBinding
import com.workdo.fashion1.remote.NetworkResponse
import com.workdo.fashion1.ui.activity.MainActivity
import com.workdo.fashion1.utils.SharePreference
import com.workdo.fashion1.utils.Utils
import kotlinx.coroutines.launch

class ActWelCome : BaseActivity() {
    private lateinit var _binding: ActWelComeBinding

    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActWelComeBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        _binding.btnLogin.setOnClickListener {
            openActivity(ActLoginOption::class.java)
        }
        _binding.btnSignup.setOnClickListener {
            openActivity(ActRegisterOption::class.java)
        }
        _binding.btnGuest.setOnClickListener {
            startActivity(
                Intent(
                    this@ActWelCome,
                    MainActivity::class.java
                )
            )
            finish()
        }

    }


}