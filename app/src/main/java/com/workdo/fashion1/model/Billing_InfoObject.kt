package com.workdo.fashion1.model

class Billing_InfoObject {
    var deliveryCountry: Int? = null
        get() = field
        set(value) {
            field = value
        }
    var firstname: String? = null
        get() = field
        set(value) {
            field = value
        }
    var deliveryAddress: String? = null
        get() = field
        set(value) {
            field = value
        }
    var billingUserTelephone: String? = null
        get() = field
        set(value) {
            field = value
        }
    var billingCountry: Int? = null
        get() = field
        set(value) {
            field = value
        }
    var billingAddress: String? = null
        get() = field
        set(value) {
            field = value
        }
    var billingPostecode: String? = null
        get() = field
        set(value) {
            field = value
        }
    var deliveryState: Int? = null
        get() = field
        set(value) {
            field = value
        }
    var billingCompanyName: String? = null
        get() = field
        set(value) {
            field = value
        }
    var lastname: String? = null
        get() = field
        set(value) {
            field = value
        }
    var billingCity: String? = null
        get() = field
        set(value) {
            field = value
        }
    var billingState: Int? = null
    var deliveryPostcode: String? = null
    var deliveryCity: String? = null
    var email: String? = null
}