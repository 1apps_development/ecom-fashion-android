package com.workdo.fashion1.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.workdo.fashion1.R
import com.workdo.fashion1.model.CategoriesModel

class AllProductCategoresAdapter(
    var context: Activity,
    private val CategoriesList: List<CategoriesModel>,
    private val onFilterClick: (String, String) -> Unit
) : RecyclerView.Adapter<AllProductCategoresAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.cell_productlisting, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val categoriesModel = CategoriesList[position]
        if (categoriesModel.id==0){
            holder.card.background =
                ResourcesCompat.getDrawable(context.resources, R.drawable.bglightpinkr_10, null)
            holder.tvProductName.setTextColor(
                AppCompatResources.getColorStateList(
                    context, R.color.appcolor
                )
            )
        }else{
            holder.card.background =
                ResourcesCompat.getDrawable(context.resources, R.drawable.bgappcolor_15, null)
            holder.tvProductName.setTextColor(
                AppCompatResources.getColorStateList(
                    context, R.color.white
                )
            )
        }
        holder.tvProductName.text=categoriesModel.cateName

    }

    override fun getItemCount(): Int {
        return CategoriesList.size
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val tvProductName: TextView = itemView.findViewById(R.id.tvProductName)
        val card: ConstraintLayout = itemView.findViewById(R.id.card)
    }

    private fun onFilterClick(id: String, name: String) {
        onFilterClick.invoke(id, name)
    }
}