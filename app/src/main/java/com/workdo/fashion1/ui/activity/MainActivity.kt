package com.workdo.fashion1.ui.activity

import android.app.AlertDialog
import android.util.Log
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.workdo.fashion1.R
import com.workdo.fashion1.base.BaseActivity
import com.workdo.fashion1.databinding.ActivityMainBinding
import com.workdo.fashion1.ui.authentication.ActWelCome
import com.workdo.fashion1.ui.fragment.*
import com.workdo.fashion1.utils.SharePreference
import com.workdo.fashion1.utils.Utils

class MainActivity : BaseActivity() {
    private lateinit var _binding: ActivityMainBinding
    var pos = ""
    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActivityMainBinding.inflate(layoutInflater)

        val name = SharePreference.getStringPref(this@MainActivity, SharePreference.userName)
        Log.e("name", name.toString())
        pos = intent.getStringExtra("pos").toString()
        Log.e("Pos", pos)
        if(SharePreference.getStringPref(this@MainActivity,SharePreference.cartCount).isNullOrEmpty())
        {
            SharePreference.setStringPref(this@MainActivity,SharePreference.cartCount,"0")
        }

        if (pos == "1") {
            setCurrentFragment(FragHome())
            _binding.bottomNavigation.selectedItemId = R.id.ivBestSellers
        } else if (pos == "2") {
            setCurrentFragment(FragProduct())
            _binding.bottomNavigation.selectedItemId = R.id.ivProduct
        } else if (pos == "3") {
            setCurrentFragment(FragAllCategories())
            _binding.bottomNavigation.selectedItemId = R.id.ivCategories
        } else if (pos == "4") {
            setCurrentFragment(FragWishList())
            _binding.bottomNavigation.selectedItemId = R.id.ivWishList
        } else if (pos == "5") {
            setCurrentFragment(FragSetting())
            _binding.bottomNavigation.selectedItemId = R.id.ivSettings
        } else {
            setCurrentFragment(FragHome())
        }
        bottomSheetItemNavigation()
    }

    private fun bottomSheetItemNavigation() {
        _binding.bottomNavigation.setOnItemSelectedListener { item ->
            val fragment = supportFragmentManager.findFragmentById(R.id.mainContainer)

            when (item.itemId) {
                R.id.ivBestSellers -> {
                    if (fragment !is FragHome) {
                        supportFragmentManager.fragments.clear()
                        setCurrentFragment(FragHome())
                    }
                    true
                }
                R.id.ivProduct -> {
                    if (fragment !is FragProduct) {
                        supportFragmentManager.fragments.clear()
                        setCurrentFragment(FragProduct())
                    }
                    true
                }
                R.id.ivCategories -> {

                    if (fragment !is FragAllCategories) {
                        supportFragmentManager.fragments.clear()
                        setCurrentFragment(FragAllCategories())
                        SharePreference.setStringPref(this@MainActivity,SharePreference.clickMenu,"click")
                    }else
                    {
                        fragment.showLayout()
                    }
                    true

                }
                R.id.ivWishList -> {
                    if (Utils.isLogin(this@MainActivity)) {
                        if (fragment !is FragWishList) {
                            supportFragmentManager.fragments.clear()
                            setCurrentFragment(FragWishList())
                        }
                    } else {
                        openActivity(ActWelCome::class.java)

                    }

                    true
                }
                R.id.ivSettings -> {
                    if (Utils.isLogin(this@MainActivity)) {
                        if (fragment !is FragSetting) {
                            supportFragmentManager.fragments.clear()
                            setCurrentFragment(FragSetting())
                        }
                    } else {
                        openActivity(ActWelCome::class.java)

                    }


                    true
                }


                else -> {
                    false
                }
            }
        }
    }


    private fun setCurrentFragment(fragment: Fragment) =
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.mainContainer, fragment)
            commit()
        }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 1) {
            finishAffinity()
        } else {
            mExitDialog()
        }
    }

    private fun mExitDialog() {
        val builder = AlertDialog.Builder(this@MainActivity)
        builder.setTitle(R.string.app_name)
        builder.setMessage(R.string.exit)
        builder.setPositiveButton(getString(R.string.yes)) { dialogInterface, which ->
            builder.setCancelable(true)
            ActivityCompat.finishAfterTransition(this@MainActivity)
            ActivityCompat.finishAffinity(this@MainActivity);
            finish()
        }
        builder.setNegativeButton(getString(R.string.no)) { dialogInterface, which ->
            builder.setCancelable(true)
        }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

}