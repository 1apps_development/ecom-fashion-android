package com.workdo.fashion1.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.fashion1.R
import com.workdo.fashion1.api.ApiClient
import com.workdo.fashion1.databinding.CellLoyalityBinding
import com.workdo.fashion1.databinding.CellOrderHistoryBinding
import com.workdo.fashion1.databinding.CellWishlistBinding
import com.workdo.fashion1.model.AddressListData
import com.workdo.fashion1.model.OrderListData
import com.workdo.fashion1.model.WishListDataItem
import com.workdo.fashion1.utils.Constants
import com.workdo.fashion1.utils.SharePreference
import com.workdo.fashion1.utils.Utils

class LoyalitylistAdapter(
    private val context: Activity,
    private val orderlist: ArrayList<OrderListData>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<LoyalitylistAdapter.WishlistViewHolder>() {
    var currency = SharePreference.getStringPref(context, SharePreference.currency_name)

    inner class WishlistViewHolder(private val binding: CellLoyalityBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: OrderListData,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {
            binding.tvDate.text =
                context.getString(R.string.date).plus(" ").plus(Utils.getDate(data.date.toString()))
            binding.tvOrderId.text = "#".plus(data.productOrderId)
            binding.tvFeaturedProductPrice.text ="+".plus(" ").plus(Utils.getPrice(data.rewardPoints.toString())).plus(" ").plus(
                currency)

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WishlistViewHolder {
        val view =
            CellLoyalityBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return WishlistViewHolder(view)
    }

    override fun onBindViewHolder(holder: WishlistViewHolder, position: Int) {
        holder.bind(orderlist[position], context, position, itemClick)
    }

    override fun getItemCount(): Int {

        return orderlist.size
    }
}